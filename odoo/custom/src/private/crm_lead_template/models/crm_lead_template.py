# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo import fields, models

from odoo.addons.crm.models.crm_stage import AVAILABLE_PRIORITIES


class CrmLeadTemplate(models.Model):
    _name = "crm.lead.template"
    _description = "Lead Template"
    _order = "sequence, name"

    sequence = fields.Integer(
        default=10,
    )
    name = fields.Char(
        required=True,
    )
    active = fields.Boolean(
        default=True,
    )
    description = fields.Html()
    priority = fields.Selection(
        selection=AVAILABLE_PRIORITIES,
        default=AVAILABLE_PRIORITIES[0][0],
    )
    tag_ids = fields.Many2many(
        comodel_name="crm.tag",
        string="Tags",
    )
    team_id = fields.Many2one(
        comodel_name="crm.team",
        string="Sales Team",
    )
    campaign_id = fields.Many2one(
        comodel_name="utm.campaign",
        string="Campaign",
    )
    medium_id = fields.Many2one(
        comodel_name="utm.medium",
        string="Medium",
    )
    source_id = fields.Many2one(
        comodel_name="utm.source",
        string="Source",
    )
