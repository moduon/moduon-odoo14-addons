# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo import api, fields, models


class CRMLead(models.Model):
    _inherit = "crm.lead"

    lead_template_id = fields.Many2one(
        comodel_name="crm.lead.template",
        string="Template",
    )

    @api.onchange("lead_template_id")
    def _compute_lead_template_id(self):
        """Sets the fields of the lead Lead Template"""
        if self.lead_template_id:
            self.name = ": ".join(
                list(filter(bool, [self.lead_template_id.name, self.name]))
            )
            # No sobreescribir nada
            if self.lead_template_id.priority:
                self.priority = self.lead_template_id.priority
            if self.lead_template_id.tag_ids:
                self.tag_ids = [(4, t.id) for t in self.lead_template_id.tag_ids]
            if self.lead_template_id.description:
                new_description = self.lead_template_id.description
                if self.description:
                    new_description = self.description + new_description
                self.description = new_description
            if self.lead_template_id.team_id:
                self.team_id = self.lead_template_id.team_id
            if self.lead_template_id.campaign_id:
                self.campaign_id = self.lead_template_id.campaign_id
            if self.lead_template_id.medium_id:
                self.medium_id = self.lead_template_id.medium_id
            if self.lead_template_id.source_id:
                self.source_id = self.lead_template_id.source_id
