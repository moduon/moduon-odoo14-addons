# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from markupsafe import Markup

from odoo.tests import Form, common, tagged
from odoo.tools.safe_eval import safe_eval


@tagged("post_install", "-at_install")
class TestCrmLead(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.utm_campaign = self.env.ref("utm.utm_campaign_email_campaign_services")
        self.utm_medium = self.env.ref("utm.utm_medium_email")
        self.utm_source = self.env.ref("utm.utm_source_mailing")
        self.team_sales = self.env.ref("sales_team.team_sales_department")
        self.crm_tag = self.env["crm.tag"].create({"name": "Test tag"})
        self.lead_template_description = "<p>Customer wants some stuff.</p>"
        self.lead_template = self.env["crm.lead.template"].create(
            {
                "name": "Wants some stuff",
                "sequence": 1,
                "priority": "3",
                "tag_ids": [(4, self.crm_tag.id)],
                "team_id": self.team_sales.id,
                "campaign_id": self.utm_campaign.id,
                "medium_id": self.utm_medium.id,
                "source_id": self.utm_source.id,
                "description": self.lead_template_description,
            }
        )

    def test_lead_template(self):
        """Checks that the Lead Template fields are set on the Lead"""
        ctx = safe_eval(self.env.ref("crm.crm_lead_all_leads").read()[0]["context"])
        lead_name = "Test Lead Name"
        lead_form = Form(self.env["crm.lead"].with_context(**ctx))
        lead_form.name = lead_name
        lead_form.partner_name = "Test Company"
        lead_form.contact_name = "Test Contact"
        lead_form.lead_template_id = self.lead_template
        lead = lead_form.save()
        self.assertEqual(lead.priority, "3")
        self.assertEqual(lead.team_id, self.team_sales)
        self.assertEqual(lead.campaign_id, self.utm_campaign)
        self.assertEqual(lead.medium_id, self.utm_medium)
        self.assertEqual(lead.source_id, self.utm_source)
        self.assertEqual(lead.description, Markup(self.lead_template_description))
        self.assertIn(self.crm_tag, lead.tag_ids)
        self.assertEqual(": ".join([self.lead_template.name, lead_name]), lead.name)
