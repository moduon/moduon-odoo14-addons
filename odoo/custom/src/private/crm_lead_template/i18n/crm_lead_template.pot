# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* crm_lead_template
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 15.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-17 15:35+0000\n"
"PO-Revision-Date: 2023-03-17 15:35+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__active
msgid "Active"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "Archived"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__campaign_id
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Campaign"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "Classification"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.actions.act_window,help:crm_lead_template.crm_lead_template_action
msgid "Click to create a new lead or opportunity template."
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__create_uid
msgid "Created by"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__create_date
msgid "Created on"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__description
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "Description"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__display_name
msgid "Display Name"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_view_form
msgid "Fill with Template..."
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields.selection,name:crm_lead_template.selection__crm_lead_template__priority__2
msgid "High"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "High priority"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__id
msgid "ID"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Inactive"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template____last_update
msgid "Last Modified on"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__write_uid
msgid "Last Updated by"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__write_date
msgid "Last Updated on"
msgstr ""

#. module: crm_lead_template
#: model:ir.model,name:crm_lead_template.model_crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "Lead Template"
msgstr ""

#. module: crm_lead_template
#: model:ir.actions.act_window,name:crm_lead_template.crm_lead_template_action
#: model:ir.ui.menu,name:crm_lead_template.menu_crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_tree
msgid "Lead Templates"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.actions.act_window,help:crm_lead_template.crm_lead_template_action
msgid ""
"Lead or Opportunity Templates will help you to fill fast leads and opportunities.<br>\n"
"                Also provides some homogeneous data to your leads/opportunities."
msgstr ""

#. module: crm_lead_template
#: model:ir.model,name:crm_lead_template.model_crm_lead
msgid "Lead/Opportunity"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "Lead/Opportunity internal notes"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields.selection,name:crm_lead_template.selection__crm_lead_template__priority__0
msgid "Low"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Low priority"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__medium_id
#: model:ir.model.fields.selection,name:crm_lead_template.selection__crm_lead_template__priority__1
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Medium"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__name
msgid "Name"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Normal priority"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__priority
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Priority"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__team_id
msgid "Sales Team"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__sequence
msgid "Sequence"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__source_id
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Source"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead_template__tag_ids
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Tags"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields,field_description:crm_lead_template.field_crm_lead__lead_template_id
msgid "Template"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "To add in lead name..."
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_form
msgid "UTM"
msgstr ""

#. module: crm_lead_template
#: model:ir.model.fields.selection,name:crm_lead_template.selection__crm_lead_template__priority__3
msgid "Very High"
msgstr ""

#. module: crm_lead_template
#: model_terms:ir.ui.view,arch_db:crm_lead_template.crm_lead_template_view_search
msgid "Very High priority"
msgstr ""
