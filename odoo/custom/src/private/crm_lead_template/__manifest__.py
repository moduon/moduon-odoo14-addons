# Copyright 2023 Moduon Team S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl-3.0)

{
    "name": "CRM Lead Template",
    "summary": "Templates for CRM Leads and Opportunities",
    "version": "16.0.0.1.0",
    "development_status": "Alpha",
    "category": "Sales/CRM",
    "website": "https://github.com/OCA/crm",
    "author": "Moduon, Odoo Community Association (OCA)",
    "maintainers": ["Shide"],
    "license": "AGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "crm",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/crm_lead_template_view.xml",
        "views/crm_lead_view.xml",
        "data/menu.xml",
    ],
    "demo": [
        "demo/crm_lead_template_demo.xml",
    ],
}
