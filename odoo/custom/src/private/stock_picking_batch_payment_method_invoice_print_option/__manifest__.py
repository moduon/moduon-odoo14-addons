# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Stock Picking Batch Payment Method Invoice Print Option",
    "summary": "Print invoices with a specific payment method from stock picking "
    "batchs",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Inventory",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "stock_picking_batch_print_invoices",
        "account_payment_method",
    ],
    "data": [
        "views/stock_picking_type.xml",
        "views/report_picking_batch_print_invoices.xml",
    ],
}
