To use this module, you need to:

To use this module, you need to:

1. Go to Sales > Orders > Customers.
2. Create new customer.
3. Select sale payment method in created customer. This payment method must be
   configured in operation type.
4. Go to Sales > Orders > Quotations.
5. Create new quotations to created customer before with storable products.
6. Confirm quotation.
7. Go to Inventory > Operations > Batch Transfers.
8. Create new batch and add deliveries if the orders confirmed before.
9. Validate Batch.
10. Go to Sales > To Invoice > Orders to Invoice.
11. Select orders confirmed before and create invoices.
12. Go to Inventory > Operations > Batch Transfers.
13. Select batch created before.
14. Click on Delivery Slips / Invoices.
15. All delivery slips and invoices with payment methods configured in operation type of
    the pickings in the batch will be printed as many time as configured.
