Improvements on Stock UX:

- Added Product Tags menu under Inventory \> Configuration \> Products
- Added sequence on Stock Locations
- Added Product Barcode column on stock report
- Added positions on Location views
- Sort Stock Move Lines by its own defined \_order
- Added filter My products where user is responsible in following models:
  - Product (for example in Inventory \> Products \> Product).
  - Product Packaging (for example in Inventory \> Configuration \> Product Packagings,
    after activating Product Packagings in Inventory Settings).
  - Stock Lot (for example in Inventory \> Products \> Lots/Serial Numbers, after
    activating Lots & Serial Numbers in Inventory Settings).
  - Stock Move (for example in Inventory \> Reporting \> Stock Moves, only if you have
    Inventory Administrator access).
  - Stock Move Line (for example in Inventory \> Reporting \> Moves History, only if you
    have Inventory Administrator access).
  - Stock Order Point (for example in Inventory \> Operations \> Replenishment, , only
    if you have Inventory Administrator access).
  - Stock Picking (for example in menu Inventory \> Operations \> Transfers).
  - Stock Quant (for example in Inventory \> Operations \> Inventory Adjustments).
  - Stock Quant Package (for example in Inventory \> Operations \> Product Package,
    after activating Packages in Inventory Settings).
  - Stock Scrap (for example in Inventory \> Operations \> Scrap).
- Add quick search on following models:

> - Product (for example in Inventory \> Products \> Product).
> - Stock Lot (for example in Inventory \> Products \> Lots/Serial Numbers, after
>   activating Lots & Serial Numbers in Inventory Settings).
> - Stock Move (for example in Inventory \> Reporting \> Stock Moves, only if you have
>   Inventory Administrator access).
> - Stock Move Line (for example in Inventory \> Reporting \> Moves History, only if you
>   have Inventory Administrator access).
> - Stock Order Point (for example in Inventory \> Operations \> Replenishment, only if
>   you have Inventory Administrator access).
> - Stock Quant (for example in Inventory \> Operations \> Inventory Adjustments).
> - Stock Scrap (for example in Inventory \> Operations \> Scrap).

- Add group by product responsible on following models:
  - Stock Lot (for example in Inventory \> Products \> Lots/Serial Numbers, after
    activating Lots & Serial Numbers in Inventory Settings).
  - Stock Move (for example in Inventory \> Reporting \> Stock Moves, only if you have
    Inventory Administrator access).
  - Stock Move Line (for example in Inventory \> Reporting \> Moves History, only if you
    have Inventory Administrator access).
  - Stock Order Point (for example in Inventory \> Operations \> Replenishment, , only
    if you have Inventory Administrator access).
  - Stock Quant (for example in Inventory \> Operations \> Inventory Adjustments).
  - Stock Scrap (for example in Inventory \> Operations \> Scrap).
- Added storage category from location in stock move (Activating storage Locations in
  Inventory Settings and only if you have Manage Storage Category group). Theses fields
  are computed with the categories of origin and destination locations. Added fields in
  tree views and searchs to quick search and group by.
- New search in Product Packaging, allow search by product packing name and product
  name.
- Filters Today and Tomorrow by date_deadline in Stock Picking (for example in menu
  Inventory \> Operations \> Transfers).
- Add group by date deadline in Stock Picking (for example in menu Inventory \>
  Operations \> Transfers).
- Added destiation fields (zip, city, state, country) in Stock Picking tree view
- Hide by default (but allow showing) stock package fields in Stock Move Line tree
  views.
- Added Qty, UoM and Note in Lots tree view.
- Shorten string of _Unit of Measuer_ to _UoM_ in picking operations tree.

- Picking Operations report:
  - Tables has smaller Font Size (13px)
  - Move picking barcode next to the picking name
  - Display suggested locations to move products on incoming shipments
  - Display Vendor product code and name
- Delivery Slip report:
  - Added picking barcode
  - Display date (instead of datetime) on Shipping Date
