from odoo import api, fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    location_storage_category_id = fields.Many2one(
        "stock.storage.category",
        compute="_compute_location_storage_category_id",
        store=True,
    )
    location_dest_storage_category_id = fields.Many2one(
        "stock.storage.category",
        compute="_compute_location_dest_storage_category_id",
        store=True,
    )
    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )

    @api.depends("location_id.storage_category_id")
    def _compute_location_storage_category_id(self):
        for record in self:
            record.location_storage_category_id = record.location_id.storage_category_id

    @api.depends("location_dest_id.storage_category_id")
    def _compute_location_dest_storage_category_id(self):
        for record in self:
            record.location_dest_storage_category_id = (
                record.location_dest_id.storage_category_id
            )

    @api.depends("product_id.responsible_id")
    def _compute_product_responsible_id(self):
        for move in self:
            move.product_responsible_id = move.product_id.responsible_id
