from odoo import api, fields, models


class StockQuant(models.Model):
    _inherit = "stock.scrap"

    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )

    @api.depends("product_id.responsible_id")
    def _compute_product_responsible_id(self):
        for scrap in self:
            scrap.product_responsible_id = scrap.product_id.responsible_id
