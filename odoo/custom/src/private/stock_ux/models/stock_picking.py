from odoo import fields, models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    destination_zip = fields.Char(
        related="partner_id.zip",
        string="Destination Zip",
    )
    destination_city = fields.Char(
        related="partner_id.city",
        string="Destination City",
    )
    destination_state_id = fields.Many2one(
        related="partner_id.state_id",
        string="Destination State",
    )
    destination_country_id = fields.Many2one(
        related="partner_id.country_id",
        string="Destination Country",
    )
