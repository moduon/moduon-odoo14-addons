from odoo import fields, models


class StockLocation(models.Model):
    _inherit = "stock.location"
    _order = "sequence, complete_name, id"

    sequence = fields.Integer(
        default=9999,
        help="Sequence of the location when displaying a list of locations.\n"
        "Used in some reports to order moves or move lines by location.",
    )
