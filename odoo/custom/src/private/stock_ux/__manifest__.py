# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Stock UX",
    "version": "16.0.1.0.2",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Inventory/Inventory",
    "depends": ["stock"],
    "data": [
        "views/stock_move_line_view.xml",
        "views/stock_location_view.xml",
        "views/product_template_view.xml",
        "views/stock_lot_views.xml",
        "views/stock_move_view.xml",
        "views/stock_orderpoint_views.xml",
        "views/stock_picking_views.xml",
        "views/stock_quant_views.xml",
        "views/stock_scrap_views.xml",
        "views/product_packaging_views.xml",
        # Reports
        "views/report_delivery_document.xml",
        "report/report_stockpicking_operations.xml",
        # Menus
        "data/menus.xml",
    ],
    "installable": False,
}
