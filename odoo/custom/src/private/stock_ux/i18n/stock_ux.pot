# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* stock_ux
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-09 08:25+0000\n"
"PO-Revision-Date: 2024-05-09 08:25+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__picking_partner_id
msgid "Contact"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__picking_partner_name
msgid "Contact name"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_internal_search_inherit_stock_ux
msgid "Date Deadline"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_picking__destination_city
msgid "Destination City"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_picking__destination_country_id
msgid "Destination Country"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__location_dest_sequence
msgid "Destination Location Sequence"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_move_search_search_inherit_stock_ux
msgid "Destination Location Storage Category"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_picking__destination_state_id
msgid "Destination State"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_picking__destination_zip
msgid "Destination Zip"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_location
msgid "Inventory Locations"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move__location_dest_storage_category_id
msgid "Location Dest Storage Category"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__location_sequence
msgid "Location Sequence"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move__location_storage_category_id
#: model_terms:ir.ui.view,arch_db:stock_ux.view_move_search_search_inherit_stock_ux
msgid "Location Storage Category"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_lot
msgid "Lot/Serial"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_warehouse_orderpoint
msgid "Minimum Inventory Rule"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.product_packaging_search
#: model_terms:ir.ui.view,arch_db:stock_ux.product_template_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.quant_package_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.quant_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.search_product_lot_filter_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_move_line_view_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_reorder_report_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_scrap_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_move_search_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_internal_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.warehouse_orderpoint_search_inherit_stock_ux
msgid "My Products"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__product_barcode
msgid "Product Barcode"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,help:stock_ux.field_stock_move_line__product_barcode
msgid "Product Barcode when the move line was created"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_move_line
msgid "Product Moves (Stock Move Line)"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_lot__product_responsible_id
#: model:ir.model.fields,field_description:stock_ux.field_stock_move__product_responsible_id
#: model:ir.model.fields,field_description:stock_ux.field_stock_move_line__product_responsible_id
#: model:ir.model.fields,field_description:stock_ux.field_stock_quant__product_responsible_id
#: model:ir.model.fields,field_description:stock_ux.field_stock_scrap__product_responsible_id
#: model:ir.model.fields,field_description:stock_ux.field_stock_warehouse_orderpoint__product_responsible_id
#: model_terms:ir.ui.view,arch_db:stock_ux.quant_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.search_product_lot_filter_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_move_line_view_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_reorder_report_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.stock_scrap_search_view_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_move_search_search_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.warehouse_orderpoint_search_inherit_stock_ux
msgid "Product Responsible"
msgstr ""

#. module: stock_ux
#: model:ir.ui.menu,name:stock_ux.menu_product_tag_config_stock
msgid "Product Tags"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_quant
msgid "Quants"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.search_product_lot_filter_inherit_stock_ux
msgid "Responsible"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_scrap
msgid "Scrap"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,field_description:stock_ux.field_stock_location__sequence
msgid "Sequence"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,help:stock_ux.field_stock_move_line__location_dest_sequence
msgid "Sequence of the destination location"
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,help:stock_ux.field_stock_location__sequence
msgid ""
"Sequence of the location when displaying a list of locations.\n"
"Used in some reports to order moves or move lines by location."
msgstr ""

#. module: stock_ux
#: model:ir.model.fields,help:stock_ux.field_stock_move_line__location_sequence
msgid "Sequence of the source location"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_move
msgid "Stock Move"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_internal_search_inherit_stock_ux
msgid "Today"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_internal_search_inherit_stock_ux
msgid "Tomorrow"
msgstr ""

#. module: stock_ux
#: model:ir.model,name:stock_ux.model_stock_picking
msgid "Transfer"
msgstr ""

#. module: stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_form_inherit_stock_ux
#: model_terms:ir.ui.view,arch_db:stock_ux.view_picking_move_tree_inherit_stock_ux
msgid "UoM"
msgstr ""
