# Copyright 2022 Moduon
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
from textwrap import dedent

from odoo import _, api, exceptions, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    missing_important_fields = fields.Char(
        string="Missing important fields",
        compute="_compute_missing_important_fields",
        store=False,
    )

    @api.model
    def _get_important_fields(self):
        return {"country_id"}

    def _compute_missing_important_fields(self):
        imp_field_data_items = self.fields_get(self._get_important_fields()).items()
        for partner in self:
            missing_fields = {
                field_values["string"]
                for field_name, field_values in imp_field_data_items
                if not partner[field_name]
            }
            partner.missing_important_fields = ", ".join(missing_fields) or False

    @api.model
    def check_access_rights(self, operation, raise_exception=True):
        unlink_group_ext_id = "partner_ux.group_allow_partner_removal"
        if (
            not self.env.is_superuser()
            and operation == "unlink"
            and not self.env.user.has_group(unlink_group_ext_id)
        ):
            if raise_exception:
                unlink_group = self.env.ref(unlink_group_ext_id)
                msg = _(
                    """
                    You are not allowed to delete '%(mname)s' (%(recs)s) records.

                    This operation is allowed for the following groups:
                    \t- %(category)s/%(group)s

                    Contact your administrator to request access if necessary.
                    """,
                    mname=self._description,
                    recs=self,
                    category=unlink_group.category_id.name,
                    group=unlink_group.name,
                )
                raise exceptions.AccessError(dedent(msg))
            return False
        return super().check_access_rights(operation, raise_exception=raise_exception)
