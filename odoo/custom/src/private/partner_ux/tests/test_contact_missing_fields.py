from odoo.tests.common import TransactionCase


class TestContactMissingFields(TransactionCase):
    def test_missing_fields(self):
        """Test `missing_important_fields` field is not empty"""
        contact = self.env["res.partner"].create({"name": "Missing fields"})
        self.assertNotEqual(contact.missing_important_fields, False)
