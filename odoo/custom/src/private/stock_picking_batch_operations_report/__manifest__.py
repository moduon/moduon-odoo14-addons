# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Stock Picking Batch Operations Report",
    "summary": "Report batch operations grouped",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Delivery",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "stock_picking_batch_ux",
        "sale_elaboration",
        "web",
    ],
    "data": [
        "views/report_batch_operations.xml",
        "report/reports.xml",
    ],
    "assets": {
        "web.report_assets_common": [
            "stock_picking_batch_operations_report/static/src/scss/**/*",
        ],
    },
}
