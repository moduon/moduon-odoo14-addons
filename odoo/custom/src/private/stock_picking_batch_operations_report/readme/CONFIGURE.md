To configure this module, you need to:

1. Go to Inventory > Configuration > Settings.
2. Activate Batch Transfers.
3. Activate Wave Transfers.
