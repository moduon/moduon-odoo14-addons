To use this module, you need to:

1. Go to Inventory > Transfers.
2. Create new transfers with same operation type.
3. Go to Inventory > Operations > Transfers.
4. Select transfers created before.
5. Click on Action > Add to batch.
6. Add to a new batch transfer.
7. Go to Inventory > Operations > Batch Transfer.
8. Select batch transfer created before.
9. Click on Print > Grouped Operations.
