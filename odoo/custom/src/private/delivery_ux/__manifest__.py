# See README.rst file on addon root folder for license details

{
    "name": "Delivery UX",
    "summary": "Add ux improvements to the delivery",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Delivery",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual", "rafaelbn"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "delivery",
    ],
    "data": [
        "views/stock_picking.xml",
    ],
}
