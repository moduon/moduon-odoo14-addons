# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def button_validate(self):
        """Allows inconsistencies in states of the same batch when validating
        a single picking in a batch. This means that pickings can be validated
        one by one without dissapearing from the batch."""
        picking_batch_map = {picking: picking.batch_id for picking in self}
        res = super().button_validate()
        for picking in self:
            if picking.state != "done":
                continue
            previous_batch = picking_batch_map[picking]
            if previous_batch and not picking.batch_id:
                previous_batch.with_context(
                    forced_allowed_picking_ids=picking.ids,
                ).picking_ids += picking
        return res
