# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo.tests import TransactionCase


class StockPickingBatchSinglePickingValidation(TransactionCase):
    @classmethod
    def setUpClass(cls):
        """Create a picking batch with two pickings from stock to customer"""
        super().setUpClass()
        cls.stock_location = cls.env.ref("stock.stock_location_stock")
        cls.supplier_location = cls.env.ref("stock.stock_location_suppliers")
        cls.customer_location = cls.env.ref("stock.stock_location_customers")
        cls.picking_type_in = cls.env["ir.model.data"]._xmlid_to_res_id(
            "stock.picking_type_in"
        )
        cls.picking_type_out = cls.env["ir.model.data"]._xmlid_to_res_id(
            "stock.picking_type_out"
        )
        cls.env["stock.picking.type"].browse(
            cls.picking_type_out
        ).reservation_method = "manual"
        cls.productA = cls.env["product.product"].create(
            {
                "name": "Product A",
                "type": "product",
                "categ_id": cls.env.ref("product.product_category_all").id,
            }
        )
        cls.productB = cls.env["product.product"].create(
            {
                "name": "Product B",
                "type": "product",
                "categ_id": cls.env.ref("product.product_category_all").id,
            }
        )

        cls.client_1 = cls.env["res.partner"].create({"name": "Client 1"})
        cls.picking_client_1 = cls.env["stock.picking"].create(
            {
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
                "picking_type_id": cls.picking_type_out,
                "partner_id": cls.client_1.id,
                "company_id": cls.env.company.id,
            }
        )

        cls.env["stock.move"].create(
            {
                "name": cls.productA.name,
                "product_id": cls.productA.id,
                "product_uom_qty": 10,
                "product_uom": cls.productA.uom_id.id,
                "picking_id": cls.picking_client_1.id,
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
            }
        )

        cls.client_2 = cls.env["res.partner"].create({"name": "Client 2"})
        cls.picking_client_2 = cls.env["stock.picking"].create(
            {
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
                "picking_type_id": cls.picking_type_out,
                "partner_id": cls.client_2.id,
                "company_id": cls.env.company.id,
            }
        )

        cls.env["stock.move"].create(
            {
                "name": cls.productB.name,
                "product_id": cls.productB.id,
                "product_uom_qty": 10,
                "product_uom": cls.productA.uom_id.id,
                "picking_id": cls.picking_client_2.id,
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
            }
        )

        cls.picking_client_3 = cls.env["stock.picking"].create(
            {
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
                "picking_type_id": cls.picking_type_out,
                "company_id": cls.env.company.id,
            }
        )

        cls.env["stock.move"].create(
            {
                "name": cls.productB.name,
                "product_id": cls.productB.id,
                "product_uom_qty": 10,
                "product_uom": cls.productA.uom_id.id,
                "picking_id": cls.picking_client_3.id,
                "location_id": cls.stock_location.id,
                "location_dest_id": cls.customer_location.id,
            }
        )

        cls.batch = cls.env["stock.picking.batch"].create(
            {
                "name": "Batch 1",
                "company_id": cls.env.company.id,
                "picking_ids": [
                    (4, cls.picking_client_1.id),
                    (4, cls.picking_client_2.id),
                ],
            }
        )

    def test_single_picking_validation(self):
        """Validating a single picking in a batch does not remove it from the batch."""
        initial_batch_picking_count = len(self.batch.picking_ids)
        self.batch.action_confirm()
        for picking in self.batch.picking_ids:
            picking.move_ids.quantity_done = picking.move_ids.product_qty
            picking.button_validate()
            self.assertEqual(len(self.batch.picking_ids), initial_batch_picking_count)
