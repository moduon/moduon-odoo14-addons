This module extends the functionality of Stock Picking and Stock Picking Batch to
support single picking validation without removing them from its batch and to allow you
to check all the picking states on the batch at any time.
