To use this module, you need to:

1. Go to a couple of Stock Pickings
2. Add them to a Batch
3. Validate them one by one (out of the batch)
4. Check every time you validate a picking, doesn't dissapear from the batch
