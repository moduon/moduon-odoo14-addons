# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Stock Picking Batch Sigle Picking Validation",
    "summary": "Allow to validate Pickings one by one without removing them from batch",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Inventory",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["Shide"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "stock_picking_batch",
    ],
    "data": [],
}
