from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    parent_partner_id = fields.Many2one(
        comodel_name="res.partner",
        compute="_compute_parent_partner_id",
        store=False,
        string="Parent Partner of the Sale Order",
    )

    def _compute_parent_partner_id(self):
        for order in self:
            order.parent_partner_id = order.partner_id.parent_id or order.partner_id
