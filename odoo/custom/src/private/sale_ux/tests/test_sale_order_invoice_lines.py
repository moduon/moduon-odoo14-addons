# Copyright 2024 Moduon Team S.L. <info@moduon.team>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

from odoo.tests import Form, tagged

from odoo.addons.account.tests.common import AccountTestInvoicingCommon


@tagged("post_install", "-at_install")
class TestSaleOrder(AccountTestInvoicingCommon):
    @classmethod
    def setUpClass(cls, chart_template_ref=None):
        super().setUpClass(chart_template_ref=chart_template_ref)
        cls.out_invoice_a = cls.init_invoice(
            "out_invoice",
            partner=cls.partner_a,
            products=cls.product_a,
            post=True,
        )

    def test_sale_order_select_invoice_lines(self):
        """Test that the invoice lines are correctly selected."""
        sale_order_form = Form(self.env["sale.order"], view="sale.view_order_form")
        sale_order_form.partner_id = self.partner_a
        with sale_order_form.order_line.new() as line:
            line.product_id = self.product_a
            line.product_uom_qty = 5.0
            # line.invoice_lines.add(self.out_invoice_a.invoice_line_ids)
        sale_order = sale_order_form.save()
        # Check move lines domain will be able to select the invoice lines
        # Same domain applied on view
        available_move_lines = self.env["account.move.line"].search(
            [
                ("move_id.partner_id", "child_of", self.partner_a.ids),
                ("sale_line_ids", "not in", sale_order.order_line.ids),
                ("display_type", "=", "product"),
                (
                    "move_id.move_type",
                    "in",
                    ("out_invoice", "out_refund", "out_receipt"),
                ),
            ]
        )
        self.assertTrue(self.out_invoice_a.invoice_line_ids in available_move_lines)
        sale_order_form = Form(sale_order, view="sale.view_order_form")
        with sale_order_form.order_line.new() as line:
            line.product_id = self.product_a
            line.product_uom_qty = 1.0
            line.invoice_lines.add(self.out_invoice_a.invoice_line_ids)
        sale_order = sale_order_form.save()
        self.assertEqual(sale_order.invoice_count, 1)
