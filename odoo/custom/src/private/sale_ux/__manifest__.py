# See README.rst file on addon root folder for license details

{
    "name": "Sale UX",
    "version": "16.0.1.0.6",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Sales/Sales",
    "depends": [
        "account",
        "sale",
        "sales_team",
    ],
    "data": [
        "views/sale_order_view.xml",
        "views/account_move_view.xml",
    ],
    "installable": False,
}
