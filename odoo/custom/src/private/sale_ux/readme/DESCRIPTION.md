Improvements on Sale UX:

- Adds Invoice Lines column on Sale Order/Order Lines
- Adds Sale Lines column on Customer based Invoices
- Sale order button lock/unlock visible for Sales Administrators
- Fields `client_order_ref` and `analytic_account_id` on sales list view
- Allows multi edit on sales quotations list view
- Tracking for Invoicing Policy on products
- Replaces date for datetime widget on the commitment_date field on Sale Quotation Tree
