# Copyright 2022 Moduon Team

{
    "name": "Jira Management - Queue",
    "summary": "Jira Management - Queue",
    "version": "16.0.1.0.0",
    "author": "Moduon",
    "website": "https://www.moduon.team/",
    "license": "LGPL-3",
    "category": "Services/Project",
    "depends": [
        "jira_management",
        "queue_job",
    ],
    "data": [
        "data/queue_data.xml",
    ],
    "installable": False,
}
