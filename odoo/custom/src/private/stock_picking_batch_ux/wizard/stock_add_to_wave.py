# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import _, api, fields, models
from odoo.exceptions import UserError


class StockPickingToWave(models.TransientModel):
    _inherit = "stock.add.to.wave"

    wave_by_group = fields.Boolean(
        string="Create wave pickings grouped by fields",
        compute="_compute_wave_by_group",
        readonly=False,
    )
    active_model = fields.Char(
        default=lambda self: self.env.context.get("active_model")
    )
    batch_group_field_id = fields.Many2one("batch.group.field")
    batch_group_field_line_ids = fields.Many2many(
        "batch.group.field.lines",
        compute="_compute_batch_group_field_line_ids",
        store="True",
        readonly=False,
    )

    @api.depends("mode")
    def _compute_wave_by_group(self):
        if self.mode == "new":
            self.wave_by_group = True

    @api.depends("batch_group_field_id")
    def _compute_batch_group_field_line_ids(self):
        for wizard in self:
            wizard.batch_group_field_line_ids = wizard.batch_group_field_id.line_ids

    def attach_pickings(self):
        self.ensure_one()
        if self.line_ids:
            if not (self.mode == "new" and self.wave_by_group):
                return super().attach_pickings()
            if not self.batch_group_field_line_ids:
                return super().attach_pickings()

            company = self.line_ids.company_id
            if len(company) > 1:
                raise UserError(
                    _("The selected operations should belong to a unique company.")
                )
            self = self.with_context(active_owner_id=self.user_id.id)
            MoveLine = self.env["stock.move.line"]
            groupby = [f.field_id.name for f in self.batch_group_field_line_ids]
            domain = [("id", "in", self.line_ids.ids)]
            move_lines_grouped = MoveLine.read_group(
                domain, groupby, groupby, lazy=False
            )
            if not move_lines_grouped:
                raise UserError(_("No operations to add to wave"))
            waves = self.env["stock.picking.batch"]
            for group in move_lines_grouped:
                wave = self.env["stock.picking.batch"].create(
                    {
                        "is_wave": True,
                        "batch_group_field_line_ids": [
                            (0, 0, {"sequence": f.sequence, "field_id": f.field_id.id})
                            for f in self.batch_group_field_line_ids
                        ],
                    }
                )
                mls = MoveLine.search(group["__domain"])
                # TODO: No debería ser necesario hacer el sudo
                mls.sudo()._add_to_wave(wave)
                waves += wave
            view = self.env.ref("stock_picking_batch.view_move_line_tree_detailed_wave")
            return {
                "name": _("Add Operations"),
                "type": "ir.actions.act_window",
                "view_mode": "list",
                "views": [(view.id, "tree")],
                "res_model": "stock.move.line",
                "domain": [
                    ("id", "in", self.line_ids.ids),
                    ("state", "!=", "done"),
                ],
                "context": dict(self.env.context, search_default_by_batch_id=1),
            }
        if self.picking_ids:
            if not (self.mode == "new" and self.wave_by_group):
                return super().attach_pickings()
            if not self.batch_group_field_line_ids:
                return super().attach_pickings()
            company = self.picking_ids.company_id
            if len(company) > 1:
                raise UserError(
                    _("The selected transfers should belong to a unique company.")
                )
        view = self.env.ref("stock_picking_batch.view_move_line_tree_detailed_wave")
        return {
            "name": _("Add Operations"),
            "type": "ir.actions.act_window",
            "view_mode": "list",
            "views": [(view.id, "tree")],
            "res_model": "stock.move.line",
            "domain": [
                ("picking_id", "in", self.picking_ids.ids),
                ("state", "!=", "done"),
            ],
            "context": dict(
                self.env.context,
                picking_to_wave=self.picking_ids.ids,
                active_wave_id=self.wave_id.id,
            ),
        }
