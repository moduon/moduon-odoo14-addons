# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import fields, models


class StockPickingType(models.Model):
    _inherit = "stock.picking.type"

    batch_group_field_id = fields.Many2one(
        "batch.group.field",
        "Default Batch Group Field",
        help="In the batches/waves of this operation type this"
        " grouping will be automatically assigned.",
    )
