# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import _, api, fields, models


class StockPickingBatch(models.Model):
    _inherit = "stock.picking.batch"

    def _get_default_weight_uom(self):
        return self.env[
            "product.template"
        ]._get_weight_uom_name_from_ir_config_parameter()

    carrier_ids = fields.Many2many(
        comodel_name="delivery.carrier",
        string="Carriers",
        help="Carries from picking in batch",
        compute="_compute_carrier_ids",
        readonly=True,
    )
    picking_count = fields.Integer(
        string="# Pickings",
        compute="_compute_picking_count",
    )
    weight = fields.Float(
        compute="_compute_weight",
    )
    weight_uom_name = fields.Char(
        string="Weight unit of measure label",
        compute="_compute_weight_uom_name",
        readonly=True,
        default=_get_default_weight_uom,
    )
    batch_group_field_id = fields.Many2one(
        "batch.group.field",
        compute="_compute_batch_group_field_id",
        store=True,
        readonly=False,
    )
    batch_group_field_line_ids = fields.Many2many(
        "batch.group.field.lines",
        compute="_compute_batch_group_field_line_ids",
        store=True,
        readonly=False,
    )
    user_id = fields.Many2one(
        compute="_compute_user_id",
        store=True,
        readonly=True,
        states={"draft": [("readonly", False)], "in_progress": [("readonly", False)]},
    )
    product_category_ids = fields.Many2many(
        "product.category", compute="_compute_product_category_ids"
    )

    @api.depends("picking_ids.carrier_id")
    def _compute_carrier_ids(self):
        for batch in self:
            batch.carrier_ids = batch.mapped("picking_ids.carrier_id")

    @api.depends("picking_ids")
    def _compute_picking_count(self):
        for batch in self:
            batch.picking_count = len(batch.picking_ids)

    @api.depends("picking_ids.weight")
    def _compute_weight(self):
        for batch in self:
            batch.weight = sum(batch.picking_ids.mapped("weight"))

    def _compute_weight_uom_name(self):
        for package in self:
            package.weight_uom_name = self.env[
                "product.template"
            ]._get_weight_uom_name_from_ir_config_parameter()

    @api.depends("batch_group_field_id")
    def _compute_batch_group_field_line_ids(self):
        for batch in self:
            batch.batch_group_field_line_ids = batch.batch_group_field_id.line_ids

    @api.depends("driver_ids")
    def _compute_user_id(self):
        for batch in self.filtered(
            lambda b: not b.is_wave and b.state in {"draft", "in_progress"}
        ):
            batch.user_id = batch.mapped("driver_ids.user_ids")[:1]

    @api.depends("picking_ids.move_ids.product_id.categ_id")
    def _compute_product_category_ids(self):
        for batch in self:
            batch.product_category_ids = batch.mapped(
                "picking_ids.move_ids.product_id.categ_id"
            )

    @api.model
    def _get_action_batch_view_pickings(self):
        t_view = self.env.ref("stock.vpicktree")
        f_view = self.env.ref("stock.view_picking_form")
        s_view = self.env.ref("stock_picking_batch_ux.view_picking_from_batch_search")
        k_view = self.env.ref("stock.stock_picking_kanban")
        c_view = self.env.ref("stock.stock_picking_calendar")
        view_list = [
            (t_view.id, "tree"),
            (f_view.id, "form"),
            (k_view.id, "kanban"),
            (c_view.id, "calendar"),
        ]
        # Add map view if available
        m_view = (
            self.env["ir.ui.view"]
            .sudo()
            .search([("model", "=", "stock.picking"), ("type", "=", "map")], limit=1)
        )
        if m_view:
            view_list.append((m_view.id, "map"))
        return {
            "name": _("Pickings"),
            "type": "ir.actions.act_window",
            "res_model": "stock.picking",
            "view_mode": "tree",
            "views": view_list,
            "target": "current",
            "search_view_id": s_view.id,
            "context": {
                "search_default_draft": False,
                "search_default_in_progress": False,
                "contact_display": "partner_address",
            },
            "domain": [],
        }

    def action_view_stock_picking(self):
        """This function returns an action that display existing pickings of
        given batch picking.
        """
        self.ensure_one()
        action = self._get_action_batch_view_pickings()
        action["domain"] += [("id", "in", self.picking_ids.ids)]
        return action

    def action_view_origin_stock_picking(self):
        action = self._get_action_batch_view_pickings()
        origin_picking_ids = self.mapped("move_ids.move_orig_ids.picking_id")
        action["name"] = _("Origin Pickings of %s Batch(es)", len(self))
        action["domain"] += [("id", "in", origin_picking_ids.ids)]
        return action

    def action_picking_origin_do_unreserve(self):
        origin_move_ids = self.mapped("move_ids.move_orig_ids").filtered_domain(
            [
                ("state", "not in", ("cancel", "done")),
            ]
        )
        origin_move_ids.mapped("picking_id").do_unreserve()
        return self.action_view_origin_stock_picking()

    def action_picking_origin_action_assign(self):
        origin_move_ids = self.mapped("move_ids.move_orig_ids").filtered_domain(
            [
                ("state", "not in", ("draft", "cancel", "done")),
            ]
        )
        origin_move_ids.mapped("picking_id").action_assign()
        return self.action_view_origin_stock_picking()

    @api.depends("picking_type_id")
    def _compute_batch_group_field_id(self):
        for batch in self:
            if batch.picking_type_id.batch_group_field_id:
                batch.batch_group_field_id = batch.picking_type_id.batch_group_field_id
