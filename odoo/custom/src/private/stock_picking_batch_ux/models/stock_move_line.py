# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import api, fields, models


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    move_dest_batch_ids = fields.Many2many(
        "stock.picking.batch",
        string="Dest Batch",
        compute="_compute_move_dest_batch_ids",
        store="True",
    )
    product_parent_category_id = fields.Many2one(
        "product.category",
        string="Product Category Parent",
        compute="_compute_product_parent_category_id",
        store=True,
    )

    # Inherited fields
    product_uom_id = fields.Many2one(string="UoM.", help="Unit of Measure")

    @api.depends("move_id.move_dest_ids.picking_id.batch_id")
    def _compute_move_dest_batch_ids(self):
        for move_line in self:
            move_line.move_dest_batch_ids = move_line.move_id.move_dest_ids.mapped(
                "picking_id.batch_id"
            ).ids

    @api.depends("product_id")
    def _compute_product_parent_category_id(self):
        for move_line in self:
            move_line.product_parent_category_id = (
                move_line._get_product_parent_category()
            )

    def _get_product_parent_category(self):
        self.ensure_one()
        categ_id = int(self.product_id.categ_id.parent_path.split("/")[0])
        return self.env["product.category"].browse(categ_id)
