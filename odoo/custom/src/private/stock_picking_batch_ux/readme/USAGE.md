To use this module, you need to:

1.  Go to Inventory \> Overview.
2.  Click on 3 points in Delivery Orders and click on "Inmediate Transfer" in New.
3.  Create some move.
4.  Click on Details in each moves and create move lines.
5.  Go to Inventory \> Overview.
6.  Click on 3 points in Delivery Orders and click on "Operations" in View.
7.  Select operations and click on "Add to wave".
8.  Select Add to a new wave transfer and check on Create wave pickings group by fields.
9.  Add fields from move lines to group by.
10. Click on "Confirm".
11. Waves are created by grouping each move line by the selected fields.
12. Go to Inventory \> Operations \> Wave Transfers.
13. Click on any wave.
14. Click on Print Batch Grouped. Report with move lines grouped by selected fields is
    printed.
