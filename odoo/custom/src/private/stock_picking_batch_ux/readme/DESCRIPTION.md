This module extends the functionality of picking batches:

> - Add new behavior in wizard to add move lines to wave. Now allow create multiple
>   waves group by move lines fields.
> - Add report to print picking batch grouped move lines are grouped by fields

    selected in batch/wave.

> - Add packaging in Operations in a batch form view.
> - Allow grouping stock move lines by contact.
> - Allow displaying stock move line partner in stock picking batch form.
> - Allow to search by move_dest_batch_ids in stock.move.lines.
> - Add filter _Has Destination Batch_ on stock.move.lines.
> - Add field carriers in stock picking batch with carriers from pickings.
> - Add carriers to stock picking batch form and tree.
> - Add picking count in stock picking batch.
> - Add smart button to open pickings in stock picking batch.
> - Add weight in stock picking batch.
> - Add weight in stock picking batch form and tree.
> - Add new model to save group by fields in wizard to create waves. Go to Inventory \>
>   Configuration \> Batch Group Field (only with developer mode).
> - An action to go to origin pickings from stock batch tree and form views.
> - An action to reserve/unreserve origin moves from stock batch tree.
> - Hide by default (but allow showing) stock package fields in Stock Move Line tree
>   views.
> - Allow to display only contact name in batch picking detailed operations.
> - Shorten _Unit of Measure_ field name in batch picking detailed operations.
> - Add _Product Packaging Qty_ field to batch picking operations.
> - Shorten string of _Product package qty_ to _Pack Qty_ in picking operations.
> - Add _Product Category Parent_ in stock move line. This field is the largest parent
>   category of the category associated with the product.
> - Add _Default Batch Group Field_ in stock picking type. This field is used to set the
>   default group by fields in batches/waves.
> - Add decoration to moves in Batch Picking
