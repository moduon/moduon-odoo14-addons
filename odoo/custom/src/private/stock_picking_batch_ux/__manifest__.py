# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Stock Picking Batch UX",
    "summary": "This module extends the functionality of picking batches",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Inventory",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "stock_picking_batch",
        "stock_ux",
        "delivery",
        "sale_elaboration",
        "delivery_driver_stock_picking_batch",
        "stock_move_packaging_qty",
        "stock_picking_operation_label",
    ],
    "data": [
        # Security
        "security/ir.model.access.csv",
        # Datas
        "data/batch_group_field_data.xml",
        # Views
        "views/stock_picking.xml",
        "views/stock_move_line.xml",
        "views/stock_picking_batch.xml",
        "views/batch_group_field.xml",
        "views/stock_picking_type.xml",
        # Wizards
        "wizard/stock_add_to_wave.xml",
    ],
}
