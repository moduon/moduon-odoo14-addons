# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo.tests import new_test_user
from odoo.tests.common import TransactionCase


class TestStockPickingBatch(TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.partner_picking_test = cls.env["res.partner"].create(
            {"name": "My Test Customer"}
        )
        cls.driver_user_1 = new_test_user(
            cls.env,
            login="driver_user_1",
            groups="base.group_user",
            name="Driver User 1",
        )
        cls.driver_user_2 = new_test_user(
            cls.env,
            login="driver_user_2",
            groups="base.group_user",
            name="Driver User 2",
        )
        cls.no_driver_user = new_test_user(
            cls.env,
            login="no_driver_user",
            groups="base.group_user",
            name="No Driver User",
        )
        cls.driver_1 = cls.driver_user_1.partner_id
        cls.driver_2 = cls.driver_user_2.partner_id
        cls.driver_3 = cls.env["res.partner"].create({"name": "Driver No user"})
        cls.product_test = cls.env["product.product"].create(
            {"name": "A product to deliver"}
        )
        cls.product_delivery_test = cls.env["product.product"].create(
            {
                "name": "Normal Delivery Charges",
                "invoice_policy": "order",
                "type": "service",
                "list_price": 10.0,
                "categ_id": cls.env.ref("delivery.product_category_deliveries").id,
            }
        )
        cls.picking_type = cls.env.ref("stock.picking_type_out")
        cls.batch_group_field_id = cls.env.ref(
            "stock_picking_batch_ux.by_uom_product_categ"
        )
        cls.delivery_1 = cls.create_delivery(cls.driver_1.id)
        cls.delivery_2 = cls.create_delivery(cls.driver_2.id)
        cls.delivery_3 = cls.create_delivery(cls.driver_3.id)
        cls.delivery_4 = cls.create_delivery(False)

    @classmethod
    def create_delivery(cls, driver_id):
        return cls.env["delivery.carrier"].create(
            {
                "name": "Normal Delivery Charges 3",
                "fixed_price": 10,
                "delivery_type": "fixed",
                "product_id": cls.product_delivery_test.id,
                "driver_id": driver_id,
            }
        )

    def create_picking(self, carrier_id):
        return self.env["stock.picking"].create(
            {
                "partner_id": self.partner_picking_test.id,
                "picking_type_id": self.picking_type.id,
                "location_id": self.env.ref("stock.stock_location_stock").id,
                "location_dest_id": self.env.ref("stock.stock_location_customers").id,
                "move_ids": [
                    (
                        0,
                        0,
                        {
                            "name": "Test",
                            "product_id": self.product_test.id,
                            "product_uom_qty": 1,
                            "product_uom": self.product_test.uom_id.id,
                            "location_id": self.env.ref(
                                "stock.stock_location_stock"
                            ).id,
                            "location_dest_id": self.env.ref(
                                "stock.stock_location_customers"
                            ).id,
                        },
                    )
                ],
                "carrier_id": carrier_id,
            }
        )

    def test_delivery_driver_stock_picking_batch(self):
        """Check Drivers in stock picking batch."""
        picking_1 = self.create_picking(self.delivery_1.id)
        picking_2 = self.create_picking(self.delivery_2.id)
        picking_3 = self.create_picking(self.delivery_3.id)
        picking_4 = self.create_picking(self.delivery_4.id)
        self.picking_type.batch_group_field_id = self.batch_group_field_id
        batch = self.env["stock.picking.batch"].create(
            {
                "name": "Test Batch",
                "picking_ids": [
                    (6, 0, (picking_1 | picking_2 | picking_3 | picking_4).ids)
                ],
            }
        )
        self.assertEqual(batch.batch_group_field_id, self.batch_group_field_id)
        self.assertEqual(batch.user_id, self.driver_user_1)
        batch.write({"picking_ids": [(3, picking_1.id)]})
        self.assertEqual(batch.user_id, self.driver_user_2)
        batch.write({"user_id": self.no_driver_user.id})
        self.assertEqual(batch.user_id, self.no_driver_user)
        batch.write({"picking_ids": [(3, picking_2.id)]})
        self.assertFalse(batch.user_id)
        batch.write({"picking_ids": [(3, picking_3.id)]})
        self.assertFalse(batch.user_id, self.no_driver_user)
        batch.write(
            {
                "picking_ids": [
                    (6, 0, (picking_1 | picking_2 | picking_3 | picking_4).ids)
                ]
            }
        )
        batch.action_confirm()
        batch.action_assign()
        batch.action_set_quantities_to_reservation()
        batch.action_done()
        batch.write({"picking_ids": [(3, picking_1.id)]})
        self.assertEqual(batch.user_id, self.driver_user_1)
