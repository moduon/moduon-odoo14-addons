# Copyright 2023 Moduon
{
    "name": "Timesheet UX",
    "summary": "Timesheet UX",
    "version": "16.0.1.0.0",
    "category": "Services/Project",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": ["sale_timesheet"],
    "data": [
        "views/project_project_views.xml",
    ],
}
