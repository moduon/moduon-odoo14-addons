# See README.rst file on addon root folder for license details

{
    "name": "Analytic UX",
    "version": "16.0.1.0.0",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Accounting/Accounting",
    "depends": [
        "analytic",
    ],
    "data": [
        "security/ir.model.access.csv",
    ],
    "uninstall_hook": "uninstall_hook",
    "installable": False,
}
