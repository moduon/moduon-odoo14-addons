import logging

from odoo import SUPERUSER_ID, api

_logger = logging.getLogger(__name__)


def uninstall_hook(cr, registry):
    """Restores Account Analytic Tag permissions"""
    env = api.Environment(cr, SUPERUSER_ID, {})
    try:
        ima_analytic_tag = env.ref("analytic.access_account_analytic_tag")
        ima_analytic_tag.sudo().perm_unlink = True
    except ValueError as ex:
        _logger.warning("Unable to restore Account Analytic Tag permission %s", ex)
