# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    visible_project_in_line = fields.Boolean(
        "Display project in line",
        compute="_compute_visible_project_in_line",
        readonly=True,
    )

    @api.depends("order_line.product_id.service_tracking")
    def _compute_visible_project_in_line(self):
        """Users should be able to select a project_id on the SO if at least one
        SO line has a product with its service tracking configured as
        'project_only'"""
        for order in self:
            order.visible_project_in_line = any(
                service_tracking == "project_only"
                for service_tracking in order.order_line.mapped(
                    "product_id.service_tracking"
                )
            ) and order.state in {"draft", "sent"}


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    editable_project = fields.Boolean(
        "Edit project", compute="_compute_editable_project", readonly=True
    )

    @api.depends("product_id.service_tracking")
    def _compute_editable_project(self):
        for line in self:
            line.editable_project = line.product_id.service_tracking == "project_only"
