This module was developed because reports for stock.move.lines doesn't exists.

It will be useful for you if you want to have a report on stock.move.lines.

This module only creates the report. Doesn't add any other functionality.

If you need this module for those reasons, these might interest you too:

- stock_barcode_print_iot_report
