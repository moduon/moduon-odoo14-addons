To use this module, you need to:

1.  Go to Inventory -\> Configuration -\> Picking Types -\> Activate show Detailed
    Operations.
2.  Go to _stock_picking_operation_label.stock_move_lines_label_view_ report and bind to
    menu.
3.  Open a view to see stock.move.lines select one and print the report.
