# Copyright 2024 Moduon Team S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl-3.0)

{
    "name": "Stock Picking Operation Label",
    "summary": "ZPL Label for Stock Picking Operations",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Inventory",
    "website": "https://gitlab.com/moduon/",
    "author": "Moduon",
    "maintainers": [],
    "license": "AGPL-3",
    "application": False,
    "installable": False,
    "preloadable": True,
    "auto_install": False,
    "depends": ["delivery_driver", "product_expiry"],
    "data": [
        "report/report_stock_picking_move_lines_view.xml",
    ],
}
