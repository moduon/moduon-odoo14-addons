from odoo.modules.module import get_resource_path
from odoo.tools import convert_xml_import


def migrate(cr, version):
    """Force update noupdate data."""
    convert_xml_import(
        cr,
        "account_payment_method",
        get_resource_path(
            "account_payment_method", "data", "account_payment_method.xml"
        ),
    )
