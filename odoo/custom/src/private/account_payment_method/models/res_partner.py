from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    property_customer_payment_method_id = fields.Many2one(
        comodel_name="account.payment.method",
        company_dependent=True,
        string="Payment Method for Customers",
        domain="[('payment_type', '=', 'inbound')]",
        tracking=True,
        help="Preferred payment method when being paid by this customer",
    )
    property_payment_method_id = fields.Many2one(
        string="Payment Method for Suppliers",
        tracking=True,
    )

    @api.model
    def _commercial_fields(self):
        payment_method_fields = [
            "property_customer_payment_method_id",
            "property_payment_method_id",
        ]
        return super()._commercial_fields() + payment_method_fields
