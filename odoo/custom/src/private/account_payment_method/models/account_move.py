from odoo import api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    preferred_payment_method_id = fields.Many2one(readonly=False)

    @api.depends("partner_id", "move_type")
    def _compute_preferred_payment_method_idd(self):
        for move in self:
            move_type = move.move_type or ""
            partner = move.partner_id.with_company(move.company_id)
            if move_type in ("out_invoice", "in_refund", "out_receipt"):
                move.preferred_payment_method_id = (
                    partner.property_customer_payment_method_id
                )
            elif move_type in ("in_invoice", "out_refund", "in_receipt"):
                # Upstream only supports this use case
                return super()._compute_preferred_payment_method_idd()
            else:
                move.preferred_payment_method_id = False

    def get_bank_accounts_to_print(self):
        self.ensure_one()
        if self.preferred_payment_method_id.print_bank_account:
            return (
                self.env["account.payment.method.line"]
                .with_company(self.company_id)
                .search(
                    [
                        ("payment_method_id", "=", self.preferred_payment_method_id.id),
                        ("company_id", "=", self.company_id.id),
                    ]
                )
                .mapped("journal_id.bank_account_id")
            )
        return False
