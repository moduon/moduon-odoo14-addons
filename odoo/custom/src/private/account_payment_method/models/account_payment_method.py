from odoo import api, fields, models


class AccountPaymentMethod(models.Model):
    _inherit = "account.payment.method"

    active = fields.Boolean(default=True)
    note = fields.Text(
        translate=True,
        help="Note to display with the payment method.",
    )
    print_bank_account = fields.Boolean(
        help="Print bank account from journal in invoice reports."
    )

    @api.model
    def _get_payment_method_information(self):
        """Fills manually created payment methods with default info.

        Mode will be set as 'multi' because it is allowed to be used in multiple
        Journals at the same time, so the user should be care about that.
        """
        information = super()._get_payment_method_information()
        payment_methods_without_information = self.search(
            [("code", "not in", list(information.keys()))]
        )
        for payment_method in payment_methods_without_information:
            information.update(
                {
                    payment_method.code: {
                        "mode": "multi",
                        "domain": [("type", "in", ("bank", "cash"))],
                    }
                }
            )
        return information
