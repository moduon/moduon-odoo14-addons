from odoo import fields, models


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    invoice_date = fields.Date(
        related="move_id.invoice_date",
        string="Invoice Date",
        readonly=True,
        store=True,
    )
