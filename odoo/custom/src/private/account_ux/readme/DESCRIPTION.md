Improvements on Account UX:

- Adds search field `amount_total_signed` on customer invoices, supplier invoices and
  account moves
- Adds field `date` (Accounting Date) to the tree view on supplier invoices
- Adds the posibility to search by `debit` and `credit` fields at the same time on
  account move lines
- Adds the posibility to search by `amount` field on payments
- Warns the user if the Bill Reference on Supplier invoice exists in other Invoice
- Adds the Bill Reference and Payment Reference on the Label of Journal Items
- Adds tax grid info in tax list view and fiscal position form view
- Account Tax Template has the same Display Name as Account Tax
- Removed widget for invoice_due_date field on account.move tree view
- Moves the Fiscal Position field after Partner on Invoices/Bills/Refunds
- Button to allow to clear all invoice lines at the same time
- Added `invoice_date` field on Move Lines. Also added to search and tree view.
