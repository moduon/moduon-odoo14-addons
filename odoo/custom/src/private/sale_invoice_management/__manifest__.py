# See README.rst file on addon root folder for license details

{
    "name": "Sale Invoice Management",
    "version": "16.0.1.0.6",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Accounting/Accounting",
    "depends": [
        "account",
        "sale",
        "sales_team",
    ],
    "data": [
        "views/account_move_views.xml",
        "data/menus.xml",
    ],
    "installable": False,
}
