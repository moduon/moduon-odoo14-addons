# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Purchase Stock UX",
    "summary": "Added some improvements related with purchase and stock modules.",
    "version": "16.0.1.0.0",
    "development_status": "Beta",
    "category": "Inventory/Purchase",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "purchase_stock",
    ],
    "data": [
        "views/product_supplierinfo_views.xml",
        "views/purchase_views.xml",
        "views/stock_orderpoint_views.xml",
        "report/purchase_report_views.xml",
        "report/purchase_quotation_templates.xml",
        "report/purchase_order_templates.xml",
    ],
}
