# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

from odoo import api, fields, models


class ProductSupplierinfo(models.Model):
    _inherit = "product.supplierinfo"

    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )

    @api.depends("product_tmpl_id.responsible_id")
    def _compute_product_responsible_id(self):
        for record in self:
            record.product_responsible_id = record.product_tmpl_id.responsible_id
