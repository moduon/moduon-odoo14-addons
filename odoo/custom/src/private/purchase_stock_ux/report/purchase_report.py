# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

from odoo import fields, models


class PurchaseReport(models.Model):
    _inherit = "purchase.report"

    product_responsible_id = fields.Many2one("res.users", "Responsible", readonly=True)

    def _select(self):
        return (
            super()._select() + ", l.product_responsible_id as product_responsible_id"
        )

    def _group_by(self):
        return super()._group_by() + ", l.product_responsible_id"
