import requests

from .common import Jira


class IssueType(Jira):
    resource_type = "issuetype"

    @Jira.manage_response
    def of_project(self, project_id, level=None):
        params = {"projectId": str(project_id), "expand": "iconUrl"}
        if level not in (None, -1, 0, 1):
            raise Exception("Invalid level")
        params.update({"level": level})

        return requests.get(
            self._get_base_resource_url("project"),
            headers=self._headers,
            auth=self._authorization,
            params=params,
            timeout=30,
        )
