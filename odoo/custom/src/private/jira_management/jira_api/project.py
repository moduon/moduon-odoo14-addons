import requests

from odoo import _

from .common import Jira


class Project(Jira):
    resource_type = "project"

    def _manage_response_exception(self, response):
        if response.status_code == 409:
            raise Exception(
                _("Requested Workflow Scheme could not be assigned to created project")
            )
        return super()._manage_response_exception(response)

    # pylint: disable=W8106
    @Jira.manage_response
    def create(
        self,
        key,
        name,
        project_type=None,
        project_template=None,
        description=None,
        lead_account=None,
        assignee="UNASSIGNED",
        avatar=None,
        issue_security_scheme=None,
        permission_scheme=None,
        notification_scheme=None,
        workflow_scheme=None,
        category=None,
    ):
        if self._company.jira_default_project_employee_id:
            lead_account = (
                self._company.jira_default_project_employee_id.jira_account_id
                or lead_account
            )
        data = {
            "key": key,
            "name": name,
            "projectTypeKey": project_type or self._project_type,
            "projectTemplateKey": project_template or self._project_template,
            "description": description and description.unescape() or None,
            "leadAccountId": lead_account,  # account_id
            # "url": self._url,
            "assigneeType": assignee,
            "avatarId": avatar,
            "issueSecurityScheme": issue_security_scheme,
            "permissionScheme": permission_scheme,
            "notificationScheme": notification_scheme,
            "categoryId": category,
        }
        return requests.post(
            self._get_base_resource_url(),
            headers=self._headers,
            auth=self._authorization,
            json=data,
            timeout=30,
        )

    @Jira.manage_response
    def delete(self, key):
        return requests.delete(
            self._get_base_resource_url(resource=key),
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_response
    def update(
        self,
        key=None,
        name=None,
        project_type=None,
        project_template=None,
        description=None,
        lead_account=None,
        assignee=None,
        avatar=None,
        issue_security_scheme=None,
        permission_scheme=None,
        notification_scheme=None,
        workflow_scheme=None,
        category=None,
    ):
        data = {
            "key": key,
            "name": name,
            "projectTypeKey": project_type,
            "projectTemplateKey": project_template,
            "description": description and description.unescape() or None,
            "leadAccountId": lead_account,  # account_id
            # "url": self._url,
            "assigneeType": assignee,
            "avatarId": avatar,
            "issueSecurityScheme": issue_security_scheme,
            "permissionScheme": permission_scheme,
            "notificationScheme": notification_scheme,
            "categoryId": category,
        }
        return requests.put(
            self._get_base_resource_url(resource=key),
            headers=self._headers,
            auth=self._authorization,
            data=data,
            timeout=30,
        )

    @Jira.manage_response
    def get(self, key, expand="description,issueTypes,lead,issueTypeHierarchy"):
        return requests.get(
            self._get_base_resource_url(resource=key),
            params={"expand": expand},
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_response
    def statuses(self, key):
        return requests.get(
            self._get_base_resource_url("statuses", resource=key),
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_paginated_response("issues")
    def issues(self, key, date_updated=None, **kwargs):
        jql = f"project = {key}"
        if date_updated:
            ddupd = date_updated.strftime("%Y-%m-%d %H:%M")
            jql += f" AND updated >= {ddupd!r}"
        return requests.get(
            self._base_resource_url + "/search",
            headers=self._headers,
            auth=self._authorization,
            params=dict(jql=jql, **kwargs),
            timeout=30,
        )
