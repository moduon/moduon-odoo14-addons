class RequestNotValidException(Exception):
    """Request Not Valid exception for Jira"""


class UserNotLoggedIndException(Exception):
    """User Not Logged In exception for Jira"""


class ResourceNotFoundException(Exception):
    """Resource Not Found exception for Jira"""


class AccessRightsException(Exception):
    """Access Rights exception for Jira"""
