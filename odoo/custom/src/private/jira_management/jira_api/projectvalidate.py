import requests

from .common import Jira


class ProjectValidate(Jira):
    resource_type = "projectvalidate"

    @Jira.manage_response
    def valid_key(self, key):
        return requests.get(
            self._get_base_resource_url("validProjectKey"),
            headers=self._headers,
            auth=self._authorization,
            params={"key": key},
            timeout=30,
        )

    @Jira.manage_response
    def valid_name(self, name):
        return requests.get(
            self._get_base_resource_url("validProjectName"),
            headers=self._headers,
            auth=self._authorization,
            params={"name": name},
            timeout=30,
        )
