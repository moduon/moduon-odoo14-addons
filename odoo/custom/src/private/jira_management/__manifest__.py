# Copyright 2022 Moduon Team

{
    "name": "Jira Management",
    "summary": "Jira Management",
    "version": "16.0.1.0.0",
    "author": "Moduon",
    "website": "https://www.moduon.team/",
    "license": "LGPL-3",
    "category": "Services/Project",
    "depends": [
        "project",
        "hr",
        "hr_timesheet",
    ],
    "external_dependencies": {
        "python": ["requests"],
    },
    "data": [
        # Data
        "security/ir.model.access.csv",
        "data/ir_cron.xml",
        "data/project_template_jira_data.xml",
        # Views
        "views/project_template_jira_views.xml",
        "views/res_config_settings_views.xml",
        "views/project_task_type_views.xml",
        "views/project_project_views.xml",
        "views/project_task_views.xml",
        "views/hr_employee_views.xml",
        "views/account_analytic_line_views.xml",
        # Menus
        "data/menus.xml",
    ],
    "assets": {
        "web.assets_backend": ["/jira_management/static/src/css/jira.css"],
    },
    "installable": False,
}
