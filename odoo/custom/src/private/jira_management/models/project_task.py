import logging
import re

from odoo import _, api, exceptions, fields, models
from odoo.exceptions import UserError
from odoo.osv.expression import NEGATIVE_TERM_OPERATORS
from odoo.tools import groupby

from ..jira_api import jira_exceptions as jex
from ..jira_api.issue import Issue as JiraIssue
from ..jira_api.worklog import WorkLog as JiraWorkLog

_logger = logging.getLogger(__name__)


class ProjectTask(models.Model):
    _inherit = "project.task"
    _parent_store = True

    parent_path = fields.Char(index=True)
    jira_id = fields.Integer(
        string="Jira ID",
        copy=False,
        tracking=True,
    )
    jira_key = fields.Char(
        string="Jira key",
        copy=False,
        tracking=True,
    )
    jira_write_date = fields.Char(
        string="Jira Updated",
    )
    jira_url = fields.Char(
        string="Jira Link",
        compute="_compute_jira_url",
        store=True,
        readonly=True,
    )
    jira_issuetype_icon_url = fields.Char(
        string="Issuetype Icon Url",
        copy=False,
    )
    jira_issuetype_icon = fields.Html(
        string="Issuetype Icon",
        compute="_compute_jira_issuetype_icon",
        store=True,
    )
    jira_issuetype_hierarchy_level = fields.Selection(
        string="Hierarchy Level",
        selection=[
            ("1", "Epic"),
            ("0", "Task"),
            ("-1", "Subtask"),
        ],
        compute="_compute_jira_issuetype_hierarchy_level",
        store=True,
    )
    top_parent_id = fields.Many2one(
        comodel_name="project.task",
        string="Top Parent",
        compute="_compute_top_parent_id",
        store=True,
    )

    @api.depends("parent_path")
    def _compute_top_parent_id(self):
        """Compute the top parent (parent of a parent of a...) of a task"""
        for task in self:
            task.top_parent_id = self.browse(int(task.parent_path.split("/")[0]))

    @api.depends("company_id.atlassian_url", "jira_key")
    def _compute_jira_url(self):
        """Compute the Jira URL of a task"""
        for record in self:
            jira_url = None
            if record.jira_key and record.company_id.atlassian_url:
                jira_url = f"{record.company_id.atlassian_url}/browse/{record.jira_key}"
            record.jira_url = jira_url

    def action_open_on_jira(self):
        """Open Jira task in a new tab"""
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "target": "new",
            "url": self.jira_url,
        }

    @api.depends("jira_issuetype_icon_url")
    def _compute_jira_issuetype_icon(self):
        """Compute the <img> HTML code to display the Jira issuetype icon"""
        self.update({"jira_issuetype_icon": None})
        img_html = "<img src='%s' style='height:1em; vertical-align:top;'/>"
        for record in self:
            if record.jira_issuetype_icon_url:
                record.jira_issuetype_icon = img_html % record.jira_issuetype_icon_url

    @api.depends("parent_id")
    def _compute_jira_issuetype_hierarchy_level(self):
        """Compute the hierarchy level of a task in Jira"""

        def get_number_of_parents(tsk):
            n_parents = 0
            if tsk.parent_id:
                n_parents += get_number_of_parents(tsk.parent_id) + 1
            return n_parents

        for task in self:
            number_of_parents = get_number_of_parents(task)
            task.jira_issuetype_hierarchy_level = {0: "1", 1: "0"}.get(
                number_of_parents,
                "-1",
            )

    def name_get(self):
        """Displays the Jira key in the name of the task"""
        old_result, result = super().name_get(), []
        for task_id, task_name in old_result:
            task = self.browse(task_id)
            if task.jira_key:
                task_name = f"[{task.jira_key}] {task_name}"
            result.append((task_id, task_name))
        return result

    @api.model
    def name_search(self, name="", args=None, operator="ilike", limit=100):
        """Find tasks when name includes Jira key."""
        if not name:
            return super().name_search(name, args, operator, limit)
        positive = operator not in NEGATIVE_TERM_OPERATORS
        args = (args or []) + ["|" if positive else "&"]
        match = re.fullmatch(
            r"""
            \[(.+)\]   # Jira key between brackets
            \s?        # Optional space
            (.*)       # Task name
            """,
            name,
            re.VERBOSE,
        )
        if match:
            jira_key, task_name = match.groups("")
            if task_name:
                args += [
                    "&" if positive else "|",
                    ("jira_key", "=" if positive else "!=", jira_key),
                    ("name", operator, task_name),
                ]
            else:
                args += [("jira_key", "=" if positive else "!=", jira_key)]
        else:
            args += [
                "|" if positive else "&",
                ("jira_key", operator, name),
                ("name", operator, name),
            ]
        return super().name_search(name, args, operator, limit)

    @api.model
    def jira_task_domain(self, project_id: int):
        """Return common domain for Jira tasks"""
        return [
            ("project_id", "=", project_id),
            ("jira_key", "!=", False),
            ("jira_id", ">", 0),
        ]

    @api.model
    def _get_transition(self, jira_key, jira_status_category):
        """Get the transition (stage) to apply to a Jira task"""
        jira_issue_api = JiraIssue.from_company(self.company_id or self.env.company)
        transitions = jira_issue_api.available_transitions(jira_key)["transitions"]
        for transition in transitions:
            if transition["to"]["statusCategory"]["key"] == jira_status_category:
                return transition

    def create_on_jira(self):
        """Create a Jira task from a Odoo task"""
        for task in self:
            jira_issue_api = JiraIssue.from_company(task.company_id)
            issuetype = task.project_id._get_issuetypes(
                level=int(task.jira_issuetype_hierarchy_level)
            )[0]

            asignees = task.mapped("user_ids.employee_id.jira_account_id")
            result = jira_issue_api.create(
                project=task.project_id.jira_key,
                summary=task.name,
                issuetype=int(issuetype["id"]),
                # Optional
                description=task.description,
                epic_key=task.parent_id.jira_key or None,
                asignee=asignees and asignees[0] or None,
                labels=task.mapped("tag_ids.name"),
                # original_estimate=int(task.planned_hours or 0.0) or None,
                # duedate=task.date_deadline and
                # fields.Date.to_string(task.date_deadline) or None,
            )

            task.jira_key = result["key"]
            task.jira_id = int(result["id"], 0)

            # Move to other status
            if (
                task.stage_id.jira_status_category
                and task.stage_id.jira_status_category != "new"
            ):
                transition = self._get_transition(
                    task.jira_key, task.stage_id.jira_status_category
                )
                jira_issue_api.transition(result["key"], transition)

        # Update task entirely
        self._update_task_from_jira()
        return result

    def update_from_jira(self):
        """Update task from Jira including related records (not child tasks)"""
        self._update_task_from_jira()
        self._update_worklogs_from_jira()
        # self._update_comments_from_jira()

    def update_task_from_jira(self):
        """Update task from Jira"""
        return self._update_task_from_jira()

    def update_worklogs_from_jira(self):
        """Update worklogs from Jira"""
        return self._update_worklogs_from_jira()

    def update_comments_from_jira(self):
        """Update comments from Jira"""
        return self._update_comments_from_jira()

    def _update_task_from_jira(self):
        """Update task information from Jira"""
        employee_map = self.env["hr.employee"]._get_employee_jira_map(["user_id"])
        # Sort tasks by project to be ready for groupby
        self_sorted = self.sorted(key=lambda t: t.project_id.id or 0)
        for project_id, tasks in groupby(
            self_sorted, key=lambda t: t.project_id.id or 0
        ):
            current_jira_tasks_map = {
                t.jira_key: t
                for t in self.sudo()
                .with_context(active_test=False)
                .search(self.jira_task_domain(project_id))
            }
            for task in tasks:
                if not task.jira_key:
                    continue

                jira_issue_api = JiraIssue.from_company(task.company_id)
                try:
                    jira_task = jira_issue_api.get(task.jira_key)
                except jex.ResourceNotFoundException:
                    # Delete task if it does not exist in Jira
                    task._delete_from_jira()
                    continue

                # Delete task if project has changed: Let the other project handle it
                jira_fields = jira_task["fields"]
                if task.project_id.jira_key != jira_fields["project"]["key"]:
                    task._delete_from_jira()
                    continue

                task.update(
                    {
                        "jira_key": jira_task["key"],
                        "jira_id": int(jira_task["id"]),
                        "name": jira_fields["summary"],
                        "planned_hours": (
                            jira_fields["aggregatetimeoriginalestimate"] or 0.0
                        )
                        / 3600.0,
                        "date_deadline": jira_fields["duedate"],
                        "jira_issuetype_icon_url": jira_fields["issuetype"]["iconUrl"],
                        "jira_issuetype_hierarchy_level": str(
                            jira_fields["issuetype"]["hierarchyLevel"]
                        ),
                        "jira_write_date": jira_fields["updated"],
                    }
                )
                if jira_fields["assignee"]:
                    assignee_account_jira_id = jira_fields["assignee"][
                        "accountId"
                    ].split(":")[-1]
                    user = employee_map.get(assignee_account_jira_id, {}).get(
                        "user_id", [None]
                    )
                    if user and user[0]:
                        task.update({"user_ids": [(6, 0, [user[0]])]})
                # Change or update parent
                if jira_fields.get("parent"):
                    parent_task = current_jira_tasks_map.get(
                        jira_fields["parent"]["key"]
                    )
                    if parent_task and parent_task.id != task.parent_id.id:
                        task.update(
                            {
                                "parent_id": parent_task.id,
                                "display_project_id": None,
                            }
                        )
                # Status
                jira_status_categ = jira_fields["status"]["statusCategory"]["key"]
                if task.stage_id.jira_status_category != jira_status_categ:
                    new_stage = (
                        self.stage_find(
                            project_id,
                            [("jira_status_category", "=", jira_status_categ)],
                        )
                        or task.stage_id.id
                    )
                    task.update({"stage_id": new_stage})

    def _delete_from_jira(self):
        """Delete tasks if possible or hide it from Jira scope."""
        s_self = self.sudo()
        s_self.timesheet_ids.unlink()  # Delete timesheets
        try:
            # Maybe it's linked to a sale line
            return s_self.unlink()  # Delete task
        except exceptions.ValidationError as vex:
            _logger.info("Error deleting tasks %s: %s", self.ids, vex)
        # Deactivate task if cannot be deleted
        return s_self.update(
            {
                "jira_id": False,
                "jira_key": False,
                "jira_write_date": False,
                "active": False,
            }
        )

    @api.model
    def _search_text_comments(self, comment):
        """Returns the comment texts from a comment dict"""
        comment_list = []
        if comment.get("content"):
            for comment_content in comment["content"]:
                comment_list.extend(self._search_text_comments(comment_content))
        if comment.get("type") == "text":
            comment_list.append(comment.get("text"))
        return comment_list

    @api.model
    def _build_message_body(self, jira_comment_body, join_text="<br/>"):
        """Builds the message body from a Jira comment"""
        comments = []
        for comment_paragraph in jira_comment_body.get("content"):
            comments.append(
                "".join(
                    list(filter(bool, self._search_text_comments(comment_paragraph)))
                )
            )
        return join_text.join(list(filter(bool, comments)))

    def _update_comments_from_jira(self):
        """Update task comments from Jira"""
        employee_map = self.env["hr.employee"]._get_employee_jira_map()
        comment_ctx = {
            "mail_create_nosubscribe": True,
            "mail_create_nolog": True,
            "mail_post_autofollow": False,
            "mail_notify_author": False,
            "tracking_disable": True,
        }
        for task in self:
            if not task.jira_key:
                continue

            jira_issue_api = JiraIssue.from_company(task.company_id)
            try:
                jira_comments = jira_issue_api.comments(task.jira_key)["comments"]
            except jex.ResourceNotFoundException:
                continue

            jira_comment_ids = []
            for jira_comment in jira_comments:
                jira_comment_id = int(jira_comment["id"])
                jira_comment_ids.append(jira_comment_id)
                message = task.message_ids.filtered(
                    lambda mm, jira_comment_id=jira_comment_id: mm.jira_id
                    == jira_comment_id
                )
                if not message:
                    comment_author_account_jira_id = jira_comment["author"][
                        "accountId"
                    ].split(":")[-1]
                    employee = employee_map.get(comment_author_account_jira_id, {})
                    task.with_context(**comment_ctx).message_post(
                        body=self._build_message_body(jira_comment["body"]),
                        subtype_xmlid="mail.mt_comment",
                        message_type="comment",
                        email_from=employee and employee.work_email or None,
                        partner_id=employee and employee.user_id.partner_id.ids or None,
                    ).update(
                        {
                            "jira_id": int(jira_comment["id"]),
                            "jira_write_date": jira_comment["updated"],
                        }
                    )
                elif jira_comment["updated"] > message.jira_write_date:
                    message.with_context(**comment_ctx).update(
                        {
                            "body": self._build_message_body(jira_comment["body"]),
                            "jira_write_date": jira_comment["updated"],
                        }
                    )

            # Remove deleted comments
            task.message_ids.filtered(
                lambda mm, jira_comment_ids=jira_comment_ids: mm.jira_id
                not in jira_comment_ids
            ).sudo().unlink()

    def _update_worklogs_from_jira(self):
        """Update task worklogs from Jira"""
        uom_hour = self.env.ref("uom.product_uom_hour")
        employee_map = self.env["hr.employee"]._get_employee_jira_map()
        errors = []
        for task in self:
            if not task.jira_key:
                continue

            jira_issue_api = JiraIssue.from_company(task.company_id)
            try:
                jira_worklogs = jira_issue_api.worklog(task.jira_key)["worklogs"]
            except jex.ResourceNotFoundException:
                if task.timesheet_ids:
                    task.activity_schedule(
                        act_type_xmlid="mail.mail_activity_data_todo",
                        summary=_("Deleted task with timesheets"),
                        note=_(
                            "Task has been deleted on Jira and has timesheets "
                            "associated to it."
                        ),
                        user_id=task.project_id.user_id.id,
                    )
                continue

            task_timesheet_map = {}
            for timesheet in task.timesheet_ids.filtered(lambda ts: ts.jira_id):
                task_timesheet_map.update({timesheet.jira_id: timesheet})

            for jira_worklog in jira_worklogs:
                timesheet = task_timesheet_map.get(int(jira_worklog["id"]))
                if timesheet and jira_worklog["updated"] <= timesheet.jira_write_date:
                    continue  # We are up to date

                jira_worklog_author = jira_worklog.get("author", {})
                employee = employee_map.get(
                    jira_worklog_author.get("accountId", "").split(":")[-1], {}
                )
                if not employee or not employee.active:
                    raise UserError(
                        _(
                            """%(author)s Author '%(worklog)s' for Worklog
                            [%(worklog_id)s] on Task [%(task_id)s]""",
                            author=_("Missing") if not employee else _("Inactive"),
                            worklog=str(jira_worklog_author) or _("Unknown"),
                            worklog_id=jira_worklog["id"],
                            task_id=task.id,
                        )
                    )

                worklog_text = "/"
                if jira_worklog.get("comment"):
                    worklog_text = self._build_message_body(
                        jira_worklog["comment"],
                        join_text=". ",
                    )
                worklog_data = {
                    "date": jira_worklog["started"][:10],
                    "employee_id": employee.id,
                    "name": worklog_text or "/",
                    "unit_amount": jira_worklog["timeSpentSeconds"] / 3600.0,
                    "jira_id": int(jira_worklog["id"]),
                    "jira_write_date": jira_worklog["updated"],
                    "encoding_uom_id": uom_hour.id,
                    "product_uom_category_id": uom_hour.category_id.id,
                    "product_uom_id": uom_hour.id,
                }

                if not timesheet:
                    task.timesheet_ids = [(0, 0, worklog_data)]
                else:  # Checked at the start of the loop if it's up to date
                    timesheet.update(worklog_data)
            # Remove deleted workflows
            jira_worklog_keys = [int(jw["id"]) for jw in jira_worklogs]
            task.sudo().timesheet_ids.filtered(
                lambda ts, jira_worklog_keys=jira_worklog_keys: ts.jira_id
                not in jira_worklog_keys
            ).unlink()

        sorted_errors = sorted(errors, key=lambda t: (t[0], t[1]))
        for project, p_error_data in groupby(sorted_errors, key=lambda t: t[0]):
            project_errors = []
            for task, t_error_data in groupby(p_error_data, key=lambda t: t[1]):
                err_txt = ", ".join({te[2] for te in t_error_data})
                project_errors.append(f"[{task.id}] [{task.jira_key}]: {err_txt}")
            project.activity_schedule(
                activity_type_id=self.env.ref("mail.mail_activity_data_todo").id,
                date_deadline=fields.Date.today(),
                summary=_("Missing Employee from some Worklogs"),
                note=_("Affected tasks:<br/>%s", "<br/>".join(project_errors)),
                user_id=project.user_id.id,
            )

    def _get_worklogs_updated_from_jira_since(self, since):
        """Get ids of worklogs updated since"""
        jira_worklogs_updated_ids = []
        updated_worklogs = dict()
        jira_worklog_api = JiraWorkLog.from_company(self.company_id or self.env.company)
        try:
            updated_worklogs = jira_worklog_api.get_updated_worklogs_ids(since)
        except jex.ResourceNotFoundException as ex:
            raise UserError(_("Error to get updated worklogs")) from ex
        if updated_worklogs and "values" in updated_worklogs:
            jira_worklogs_updated_ids = [
                worklog["worklogId"] for worklog in updated_worklogs["values"]
            ]
        return jira_worklogs_updated_ids

    def _get_worklogs_deleted_from_jira_since(self, since):
        """Get ids of worklogs deleted since"""
        jira_worklogs_deleted_ids = []
        deleted_worklogs = dict()
        jira_worklog_api = JiraWorkLog.from_company(self.company_id or self.env.company)
        try:
            deleted_worklogs = jira_worklog_api.get_deleted_worklogs_ids(since)
        except jex.ResourceNotFoundException as ex:
            raise UserError(_("Error to get deleted worklogs")) from ex
        if deleted_worklogs and "values" in deleted_worklogs:
            jira_worklogs_deleted_ids = [
                worklog["worklogId"] for worklog in deleted_worklogs["values"]
            ]
        return jira_worklogs_deleted_ids

    @api.model
    def _update_and_delete_worklogs_from_jira_since(self, since):
        """Update and delete worklogs"""
        errors = []
        uom_hour = self.env.ref("uom.product_uom_hour")
        employee_map = self.env["hr.employee"]._get_employee_jira_map()
        # update updated worklogs from jira
        worklog_jira_ids = self._get_worklogs_updated_from_jira_since(since)
        if worklog_jira_ids:
            jira_worklog_api = JiraWorkLog.from_company(
                self.company_id or self.env.company
            )
            try:
                jira_worklogs_updated = jira_worklog_api.get_worklogs(worklog_jira_ids)
            except jex.ResourceNotFoundException as ex:
                raise UserError(
                    _(
                        "Error to get worklogs updated for worklogs: %s",
                        worklog_jira_ids,
                    )
                ) from ex
            task_worklog_map = {}
            # Sort worklogs by issueId to be ready for groupby
            jira_worklogs_updated_sorted = sorted(
                jira_worklogs_updated, key=lambda w: w["issueId"]
            )
            for issue_id, worklogs in groupby(
                jira_worklogs_updated_sorted, key=lambda w: w["issueId"]
            ):
                task_worklog_map.update({issue_id: list(worklogs)})

            existing_tasks = self.env["project.task"].search(
                [("jira_id", "in", list(task_worklog_map.keys()))]
            )

            new_worklogs = []
            for task in existing_tasks:
                for jira_worklog in task_worklog_map[str(task.jira_id)]:
                    jira_worklog_author = jira_worklog.get("author", {})
                    employee = employee_map.get(
                        jira_worklog_author.get("accountId", "").split(":")[-1], {}
                    )
                    if not employee or not employee.active:
                        _logger.warning(
                            "%s Author '%s' for Worklog [%s] on Task [%s]",
                            _("Missing") if not employee else _("Inactive"),
                            str(jira_worklog_author) or _("Unknown"),
                            jira_worklog["id"],
                            task.id,
                        )
                        errors.append(
                            (
                                task.project_id,
                                task,
                                "'{display_name}' - '{account_name}'".format(
                                    display_name=jira_worklog_author.get("displayName"),
                                    account_name=jira_worklog_author.get(
                                        "accountId", ""
                                    ).split(":")[-1],
                                ),
                            )
                        )
                        continue
                    worklog_text = "/"
                    if jira_worklog.get("comment"):
                        worklog_text = self._build_message_body(
                            jira_worklog["comment"],
                            join_text=". ",
                        )
                    worklog_data = {
                        "date": jira_worklog["started"][:10],
                        "employee_id": employee.id,
                        "name": worklog_text or "/",
                        "unit_amount": jira_worklog["timeSpentSeconds"] / 3600.0,
                        "jira_id": int(jira_worklog["id"]),
                        "jira_write_date": jira_worklog["updated"],
                        "encoding_uom_id": uom_hour.id,
                        "product_uom_category_id": uom_hour.category_id.id,
                        "product_uom_id": uom_hour.id,
                        "task_id": task.id,
                    }
                    timesheet = self.env["account.analytic.line"].search(
                        [("jira_id", "=", jira_worklog["id"])]
                    )
                    if not timesheet:
                        new_worklogs.append(worklog_data)
                    else:
                        timesheet.update(worklog_data)
            self.env["account.analytic.line"].create(new_worklogs)
        # Remove deleted worklogs from jira
        self.env["account.analytic.line"].search(
            [("jira_id", "in", self._get_worklogs_deleted_from_jira_since(since))]
        ).unlink()

        sorted_errors = sorted(errors, key=lambda t: (t[0], t[1]))
        for project, p_error_data in groupby(sorted_errors, key=lambda t: t[0]):
            project_errors = []
            for task, t_error_data in groupby(p_error_data, key=lambda t: t[1]):
                err_txt = ", ".join({te[2] for te in t_error_data})
                project_errors.append(f"[{task.id}] [{task.jira_key}]: {err_txt}")
            project.activity_schedule(
                activity_type_id=self.env.ref("mail.mail_activity_data_todo").id,
                date_deadline=fields.Date.today(),
                summary=_("Missing Employee from some Worklogs"),
                note=_("Affected tasks:<br/>%s", "<br/>".join(project_errors)),
                user_id=project.user_id.id,
            )

    _sql_constraints = [
        (
            "jira_key_unique",
            "UNIQUE(jira_key, company_id)",
            "Jira Key must be unique per company.",
        ),
    ]
