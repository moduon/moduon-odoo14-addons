from odoo import fields, models


class ProjectTaskType(models.Model):
    _inherit = "project.task.type"

    jira_status_category = fields.Selection(
        selection=[
            ("new", "New"),
            ("indeterminate", "In Progress"),
            ("done", "Done"),
        ],
        copy=False,
    )
