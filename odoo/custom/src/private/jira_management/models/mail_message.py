from odoo import fields, models


class MailMessage(models.Model):
    _inherit = "mail.message"

    jira_id = fields.Integer(string="Jira ID")
    jira_write_date = fields.Char(
        string="Jira Updated",
    )

    _sql_constraints = [
        (
            "jira_id_unique",
            "UNIQUE(jira_id, model, res_id)",
            "Jira Key must be unique per company.",
        ),
    ]
