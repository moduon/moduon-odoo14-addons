import logging
import time

from dateutil.relativedelta import relativedelta

from odoo import _, api, exceptions, fields, models

from ..jira_api.issuetype import IssueType as JiraIssueType
from ..jira_api.project import Project as JiraProject
from ..jira_api.projectvalidate import ProjectValidate as JiraProjectValidate

_logger = logging.getLogger(__name__)


class ProjectProject(models.Model):
    _inherit = "project.project"

    jira_id = fields.Integer(
        string="Jira ID",
        copy=False,
    )
    jira_key = fields.Char(
        string="Jira key",
        copy=False,
        size=10,
    )
    jira_tasks_last_sync = fields.Datetime(
        string="Last Task Synchronization",
    )
    jira_url = fields.Char(
        string="Jira Link",
        compute="_compute_jira_url",
        store=True,
        readonly=True,
    )
    jira_project_template = fields.Many2one(
        comodel_name="project.template.jira",
        copy=False,
    )
    jira_auto_sync = fields.Boolean(
        string="Jira Automatic Synchronization",
        default=True,
    )

    @api.depends("company_id.atlassian_url", "jira_key")
    def _compute_jira_url(self):
        """Computes Jira URL of the project"""
        for record in self:
            jira_url = None
            if record.jira_key and record.company_id.atlassian_url:
                jira_url = f"{record.company_id.atlassian_url}/browse/{record.jira_key}"
            record.jira_url = jira_url

    def action_open_on_jira(self):
        """Open Jira project in a new tab"""
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "target": "new",
            "url": self.jira_url,
        }

    @api.model
    def _get_projects_to_sync(self):
        """Returns projects that are configured for automatic synchronization"""
        return self.search(
            [
                ("jira_auto_sync", "=", True),
                ("jira_id", ">", 0),
                ("jira_key", "!=", False),
            ]
        )

    @api.model
    def auto_update_from_jira(self):
        """CRON function to update projects from Jira"""
        self._get_projects_to_sync().update_from_jira()

    @api.model
    def auto_update_tasks_from_jira(self):
        """CRON function to update tasks from Jira"""
        self._get_projects_to_sync().update_tasks_from_jira()

    @api.model
    def auto_update_worklogs_from_jira(self):
        """CRON function to update tasks from Jira"""
        self._get_projects_to_sync().update_worklogs_from_jira()

    def create_on_jira(self):
        """Create project on Jira"""
        usr = self.env.user
        for project in self:
            if not project.jira_key:
                raise exceptions.UserError(_("Missing Jira key"))

            # Validate key and name. If it's ok, same key and name will be returned
            jira_projectvalidate_api = JiraProjectValidate.from_company(
                project.company_id
            )
            jira_key = jira_projectvalidate_api.valid_key(project.jira_key)
            jira_name = jira_projectvalidate_api.valid_name(project.display_name)

            jira_project_api = JiraProject.from_company(project.company_id)
            lead_acc = (project.user_id or usr).sudo().employee_id.jira_account_id
            result = jira_project_api.create(
                jira_key,
                jira_name,
                project_type=project.jira_project_template.jira_type,
                project_template=project.jira_project_template.code,
                description=project.description,
                lead_account=lead_acc,
            )
            project.write(
                {
                    "jira_id": result.get("id"),
                    "jira_key": jira_key,
                    "name": jira_name,
                }
            )

    def update_from_jira(self):
        """Update project from Jira"""
        employee_map = self.env["hr.employee"]._get_employee_jira_map(["user_id"])
        for project in self:
            if not project.jira_key:
                continue

            jira_project_api = JiraProject.from_company(project.company_id)
            jira_project = jira_project_api.get(project.jira_key)
            project_data = {
                "jira_key": jira_project["key"],
                "jira_id": jira_project["id"],
                "name": jira_project["name"],
            }
            lead_account_jira_id = jira_project["lead"]["accountId"].split(":")[-1]
            user = employee_map.get(lead_account_jira_id, {}).get("user_id", [None])
            if user and user[0]:
                project_data.update({"user_id": user[0]})

            project.write(project_data)

    def create_tasks_on_jira(self):
        """Create project tasks on Jira"""
        pt_model = self.env["project.task"].sudo()
        for project in self:
            tasks_to_create = pt_model.search(
                pt_model.jira_task_domain(project.id),
                order="project_id asc, parent_id asc, id asc",
            )
            epic_tasks = tasks_to_create.filtered(
                lambda t: t.jira_issuetype_hierarchy_level == "1"
            )
            tasks = tasks_to_create.filtered(
                lambda t: t.jira_issuetype_hierarchy_level == "0"
            )
            sub_tasks = tasks_to_create.filtered(
                lambda t: t.jira_issuetype_hierarchy_level == "-1"
            )
            epic_tasks.create_on_jira()
            tasks.create_on_jira()
            sub_tasks.create_on_jira()

    def update_tasks_from_jira(self):
        """Update project tasks from Jira"""
        # Update tasks that needs to be updated
        now = fields.Datetime.now()
        tasks_new, tasks_to_delete, tasks_to_sync = self.get_tasks_from_jira()
        # 1. Update old and new tasks
        (tasks_to_sync | tasks_new).update_from_jira()
        # 2. Unlink tasks
        tasks_to_delete._delete_from_jira()
        # 3. Update sync date
        self.jira_tasks_last_sync = now

    def get_tasks_from_jira(self):
        """Create missing project tasks from Jira and returns tasks
        that has been removed in Jira and tasks that are not synced"""
        pt_model = self.env["project.task"].sudo()

        tasks_new = pt_model.browse()
        tasks_to_delete = pt_model.browse()
        tasks_to_sync = pt_model.browse()
        for project in self:
            # Calculate last sync date
            jira_update_since_dtt = None
            if (
                not self.env.context.get("full_sync", False)
                and project.jira_tasks_last_sync
            ):
                # Conver to client timezone and remove seconds and microseconds
                # because we are not passing offset on the datetime to Jira
                jira_update_since_dtt = fields.Datetime.context_timestamp(
                    project,
                    project.jira_tasks_last_sync.replace(second=0, microsecond=0),
                )
            # Get all current tasks
            current_jira_tasks_map = {
                t.jira_key: t
                for t in pt_model.with_context(active_test=False).search(
                    pt_model.jira_task_domain(project.id)
                )
            }
            # Get all updated Jira tasks since last sync
            jira_project_api = JiraProject.from_company(project.company_id)
            jira_project_issues = jira_project_api.issues(
                project.jira_key,
                date_updated=jira_update_since_dtt,
            )["issues"]

            new_tasks_data = []
            for jira_project_issue in jira_project_issues:
                # Existing task
                if jira_project_issue["key"] in current_jira_tasks_map.keys():
                    existing_task = current_jira_tasks_map[jira_project_issue["key"]]
                    # Check if it's synced
                    if (
                        not existing_task.jira_write_date
                        or jira_project_issue["fields"]["updated"]
                        > existing_task.jira_write_date
                    ):
                        tasks_to_sync |= existing_task
                    del current_jira_tasks_map[jira_project_issue["key"]]
                    continue

                # Non existing task
                new_tasks_data.append(
                    {
                        "jira_key": jira_project_issue["key"],
                        "jira_id": int(jira_project_issue["id"]),
                        "name": jira_project_issue["fields"]["summary"],
                        "project_id": project.id,
                        "user_ids": None,
                    }
                )

            # Create missing tasks
            try:
                tasks_new |= pt_model.create(new_tasks_data)
            except Exception as ex:
                _logger.warning(
                    "Error creating tasks on project %s. %s",
                    project.jira_key,
                    ex,
                )

            # Cannot check deleted tasks if we are not in full syncing
            # because we don't have all the tasks
            if self.env.context.get("full_sync", False) and current_jira_tasks_map:
                for tsk in current_jira_tasks_map.values():
                    tasks_to_delete |= tsk

        return tasks_new, tasks_to_delete, tasks_to_sync

    def _get_issuetypes(self, level=None):
        """Get Jira issue types for project. A level can be specified"""
        self.ensure_one()
        jira_issuetype_api = JiraIssueType.from_company(
            self.company_id or self.env.company
        )
        return jira_issuetype_api.of_project(
            self.jira_id,
            level=level,
        )

    def _get_status(self, issuetype, status):
        """Get Jira status data for project and issue type"""
        self.ensure_one()
        jira_project_api = JiraProject.from_company(self.company_id or self.env.company)
        for issuetype_data in jira_project_api.statuses(self.jira_key):
            if issuetype_data["id"] == issuetype:
                for status_data in issuetype_data["statuses"]:
                    if status_data["statusCategory"]["key"] == status:
                        return status_data

    def update_worklogs_from_jira(self):
        """Update and delete worklogs"""
        # take min last time sync of projects to sync and parse datetime to unix
        since_datetime = (
            min(self.mapped("jira_tasks_last_sync")) or fields.Datetime.now()
        ) - relativedelta(days=1)
        since_unix = int(time.mktime(since_datetime.timetuple()) * 1000)
        self.env["project.task"]._update_and_delete_worklogs_from_jira_since(since_unix)

    _sql_constraints = [
        (
            "jira_key_unique",
            "UNIQUE(jira_key, company_id)",
            "Jira Key must be unique per company.",
        ),
    ]
