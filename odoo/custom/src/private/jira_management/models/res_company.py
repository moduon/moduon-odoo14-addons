from odoo import fields, models


class ResCompany(models.Model):
    _inherit = "res.company"

    atlassian_url = fields.Char()
    jira_auth_mode = fields.Selection(
        string="Jira Authentication",
        selection=[
            ("basic_auth", "Basic"),
        ],
        help="Basic: Username/Password or Email/API Token",
    )
    jira_auth_username = fields.Char(
        string="Jira Username",
    )
    jira_auth_token = fields.Char(
        string="Jira API Token",
    )
    jira_default_project_type = fields.Selection(
        selection=[
            ("business", "Business"),
            ("service_desk", "Service Desk"),
            ("software", "Software"),
        ],
        string="Default Jira Project Type",
    )
    jira_default_project_template = fields.Many2one(
        comodel_name="project.template.jira",
        string="Default Jira Project Template",
    )
    jira_default_project_employee_id = fields.Many2one(
        comodel_name="hr.employee",
        string="Default Jira Project Employee",
        domain=[("jira_account_id", "!=", False)],
    )
