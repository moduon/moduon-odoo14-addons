from odoo import fields, models

from ..jira_api.user import User as JiraUser


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    jira_account_id = fields.Char(
        string="Jira Account",
        groups="hr.group_hr_user",
        tracking=True,
        copy=False,
    )

    def link_jira_user(self):
        """Link the employee to the jira account"""
        for employee in self.sudo():
            jira_user_api = JiraUser.from_company(employee.company_id)
            response = jira_user_api.search(
                query=employee.work_email or employee.private_email,
                max_results=1,
            )
            if response:
                account_id = response[0].get("accountId")
                employee.jira_account_id = account_id and account_id.split(":")[-1]

    def _get_employee_jira_map(self, fields=None):
        """Get map of employees with Jira Account"""
        if not fields:
            return {
                emp["jira_account_id"].split(":")[-1]: emp
                for emp in self.env["hr.employee"]
                .sudo()
                .with_context(active_test=False)
                .search(
                    [("jira_account_id", "!=", False)],
                )
            }
        fields = ["jira_account_id"] + fields
        return {
            empd["jira_account_id"].split(":")[-1]: {
                fname: empd[fname] for fname in fields
            }
            for empd in self.env["hr.employee"]
            .sudo()
            .with_context(active_test=False)
            .search_read(
                [("jira_account_id", "!=", False)],
                fields=fields,
            )
        }

    _sql_constraints = [
        (
            "jira_account_id_unique",
            "UNIQUE(jira_account_id, company_id)",
            "Jira Account must be unique per company.",
        ),
    ]
