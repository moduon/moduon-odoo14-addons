from unittest import mock

from odoo.tests import tagged

from ..jira_api.issue import Issue as JI
from ..jira_api.project import Project as JP
from ..jira_api.projectvalidate import ProjectValidate as JPV
from ..jira_api.worklog import WorkLog as JW
from .common import JiraCommon


@tagged("post_install", "-at_install")
class TestJiraProject(JiraCommon):
    def test_project_create_on_jira(self):
        mrv_pv = self._mock_response_values["project_validate"]
        mrv_p = self._mock_response_values["project"]
        with mock.patch.multiple(
            JPV,
            valid_key=mock.MagicMock(return_value=mrv_pv["valid_key"]),
            valid_name=mock.MagicMock(return_value=mrv_pv["valid_name"]),
        ):
            with mock.patch.object(JP, "create", return_value=mrv_p["create"]):
                prev_jira_key, prev_name = self.project.jira_key, self.project.name
                self.project.create_on_jira()
                self.assertEqual(self.project.jira_id, self.jira_project_id)
                self.assertEqual(self.project.jira_key, prev_jira_key)
                self.assertEqual(self.project.name, prev_name)

    def test_project_get_from_jira(self):
        mrv_p = self._mock_response_values["project"]
        with mock.patch.object(JP, "get", return_value=mrv_p["get"]):
            self.project.update_from_jira()
            self.assertTrue("(updated)" in self.project.name)
            self.assertEqual(self.project.user_id, self._jira_user_manager)

    def test_project_update_tasks_from_jira(self):
        mrv_p = self._mock_response_values["project"]
        mrv_i_get = {
            iss["key"]: iss for iss in self._mock_response_values["issue"]["get"]
        }
        mrv_i_worklogs = {
            wl["key"].split("__")[0]: {"worklogs": [wl]}
            for wl in self._mock_response_values["issue"]["worklog"]
        }
        mrv_i_comments = {
            ic["key"].split("__")[0]: {"comments": [ic]}
            for ic in self._mock_response_values["issue"]["comments"]
        }
        mrv_w_get_worklogs = self._mock_response_values["worklog"]["get_worklogs"]
        mrv_w_get_updated_worklogs_ids = self._mock_response_values["worklog"][
            "get_updated_worklogs_ids"
        ]
        mrv_w_get_deleted_worklogs_ids = self._mock_response_values["worklog"][
            "get_deleted_worklogs_ids"
        ]
        with mock.patch.multiple(
            JI,
            get=mock.MagicMock(side_effect=mrv_i_get.get),
            worklog=mock.MagicMock(side_effect=mrv_i_worklogs.get),
            comments=mock.MagicMock(side_effect=mrv_i_comments.get),
        ):
            with mock.patch.object(JP, "issues", return_value=mrv_p["issues"]):
                with mock.patch.multiple(
                    JW,
                    get_worklogs=mock.MagicMock(return_value=mrv_w_get_worklogs),
                    get_updated_worklogs_ids=mock.MagicMock(
                        return_value=mrv_w_get_updated_worklogs_ids
                    ),
                    get_deleted_worklogs_ids=mock.MagicMock(
                        return_value=mrv_w_get_deleted_worklogs_ids
                    ),
                ):
                    self.project.update_tasks_from_jira()
                    in_progress_tasks = list(
                        filter(
                            lambda iss: iss["fields"]["status"]["statusCategory"]["key"]
                            != "done",
                            mrv_p["issues"]["issues"],
                        )
                    )
                    self.assertEqual(self.project.task_count, len(in_progress_tasks))
                    self.assertEqual(len(self.project.task_ids[0].timesheet_ids), 1)

    def test_name_get(self):
        task = self.env["project.task"].create(
            {
                "jira_key": "TJP-1",
                "name": "Task 1",
                "project_id": self.project.id,
            }
        )
        self.assertEqual(task.name_get(), [(task.id, "[TJP-1] Task 1")])
        self.assertEqual(task.display_name, "[TJP-1] Task 1")

    def test_name_search_positive(self):
        # Create tasks from 0 to 10
        tasks = self.env["project.task"].create(
            [
                {
                    "jira_key": f"TJP-{n}",
                    "name": f"Task {n}",
                    "project_id": self.project.id,
                }
                for n in range(11)
            ]
        )
        # Check name_search finds all tasks
        self.assertItemsEqual(
            self.env["project.task"].name_search(
                args=[("project_id", "=", self.project.id)]
            ),
            self.project.tasks.name_get(),
        )
        # Check name_search when searching by jira key, name, jira key between
        # brackets, or jira key between brackets and name
        self.assertItemsEqual(
            self.env["project.task"].name_search(
                "TJP-1", args=[("project_id", "=", self.project.id)]
            ),
            [(tasks[1].id, "[TJP-1] Task 1"), (tasks[10].id, "[TJP-10] Task 10")],
        )
        self.assertItemsEqual(
            self.env["project.task"].name_search(
                "Task 1", args=[("project_id", "=", self.project.id)]
            ),
            [(tasks[1].id, "[TJP-1] Task 1"), (tasks[10].id, "[TJP-10] Task 10")],
        )
        self.assertEqual(
            self.env["project.task"].name_search(
                "[TJP-1]", args=[("project_id", "=", self.project.id)]
            ),
            [(tasks[1].id, "[TJP-1] Task 1")],
        )
        self.assertEqual(
            self.env["project.task"].name_search(
                "[TJP-1] Task", args=[("project_id", "=", self.project.id)]
            ),
            [(tasks[1].id, "[TJP-1] Task 1")],
        )
        # Same checks for name_search, but this time the operator is negative
        found_tasks = self.env["project.task"].name_search(
            "TJP-1", args=[("project_id", "=", self.project.id)], operator="not ilike"
        )
        self.assertEqual(len(found_tasks), 9)
        self.assertNotIn((tasks[1].id, "[TJP-1] Task 1"), found_tasks)
        self.assertNotIn((tasks[10].id, "[TJP-10] Task 10"), found_tasks)
        found_tasks = self.env["project.task"].name_search(
            "Task 1", args=[("project_id", "=", self.project.id)], operator="not ilike"
        )
        self.assertEqual(len(found_tasks), 9)
        self.assertNotIn((tasks[1].id, "[TJP-1] Task 1"), found_tasks)
        self.assertNotIn((tasks[10].id, "[TJP-10] Task 10"), found_tasks)
        found_tasks = self.env["project.task"].name_search(
            "[TJP-1]", args=[("project_id", "=", self.project.id)], operator="not ilike"
        )
        self.assertEqual(len(found_tasks), 10)
        self.assertNotIn((tasks[1].id, "[TJP-1] Task 1"), found_tasks)
        found_tasks = self.env["project.task"].name_search(
            "[TJP-1] Task 1",
            args=[("project_id", "=", self.project.id)],
            operator="not ilike",
        )
        self.assertEqual(len(found_tasks), 10)
        self.assertNotIn((tasks[1].id, "[TJP-1] Task 1"), found_tasks)
