from datetime import datetime
from unittest import mock

from odoo.tests import tagged

from ..jira_api.worklog import WorkLog as JW
from .common import JiraCommon


@tagged("post_install", "-at_install")
class TestTaskProject(JiraCommon):
    def test_update_and_delete_worklogs_from_jira_since(self):
        mrv_w_get_worklogs = {
            wls["key"]: wls
            for wls in self._mock_response_values["worklog"]["get_worklogs"]
        }
        mrv_w_get_updated_worklogs_ids = self._mock_response_values["worklog"][
            "get_updated_worklogs_ids"
        ]
        mrv_w_get_deleted_worklogs_ids = self._mock_response_values["worklog"][
            "get_deleted_worklogs_ids"
        ]
        with mock.patch.multiple(
            JW,
            get_worklogs=mock.MagicMock(return_value=mrv_w_get_worklogs),
            get_updated_worklogs_ids=mock.MagicMock(
                return_value=mrv_w_get_updated_worklogs_ids
            ),
            get_deleted_worklogs_ids=mock.MagicMock(
                return_value=mrv_w_get_deleted_worklogs_ids
            ),
        ):
            since = datetime.now()
            jira_worklogs_updated_ids = self.env[
                "project.task"
            ]._get_worklogs_updated_from_jira_since(since)
            self.assertEqual(type(jira_worklogs_updated_ids), list)
            jira_worklogs_deleted_ids = self.env[
                "project.task"
            ]._get_worklogs_deleted_from_jira_since(since)
            self.assertEqual(type(jira_worklogs_deleted_ids), list)
