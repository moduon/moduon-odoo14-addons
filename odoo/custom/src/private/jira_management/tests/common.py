import random

from odoo.tests import Form, TransactionCase

from odoo.addons.mail.tests.common import mail_new_test_user


class JiraCommon(TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        he_model = cls.env["hr.employee"]
        pp_model = cls.env["project.project"]
        # Create user and employee Jira Manager
        cls._jira_user_manager = mail_new_test_user(
            cls.env,
            login="jira-manager@example.com",
            groups="base.group_user,hr.group_hr_user",
            name="Jira Manager",
            email="jira-manager@example.com",
        )
        employee_form = Form(he_model.with_user(cls._jira_user_manager))
        employee_form.name = "Jira Manager"
        employee_form.work_email = cls._jira_user_manager.login
        employee_form.user_id = cls._jira_user_manager
        employee_form.jira_account_id = "test-jira-user-manager-account-id"
        cls._jira_employee_manager = employee_form.save()

        # Configure company
        cls._jira_project_template_scrum = cls.env.ref(
            "jira_management.project_template_jira_software_classic_scrum",
            raise_if_not_found=True,
        )
        cls.company = cls.env.user.company_id
        cls.company.atlassian_url = "https://test.atlassian.net"
        cls.company.jira_auth_mode = "basic_auth"
        cls.company.jira_auth_username = "test@example.com"
        cls.company.jira_auth_token = "test-token"
        cls.company.jira_default_project_type = "software"
        cls.company.jira_default_project_template = cls._jira_project_template_scrum.id
        cls.company.jira_default_project_employee_id = cls._jira_employee_manager.id

        # Create project
        cls.project = pp_model.create(
            {
                "name": "Test Jira Project",
                "jira_key": "TJP",
                "jira_project_template": cls._jira_project_template_scrum.id,
                "user_id": cls._jira_user_manager.id,
                "description": "Test Description",
                "type_ids": [
                    (
                        0,
                        0,
                        {
                            "name": "To Do",
                            "sequence": 1,
                            "fold": False,
                            "jira_status_category": "new",
                        },
                    ),
                    (
                        0,
                        0,
                        {
                            "name": "In Progress",
                            "sequence": 2,
                            "fold": False,
                            "jira_status_category": "indeterminate",
                        },
                    ),
                    (
                        0,
                        0,
                        {
                            "name": "Done",
                            "sequence": 3,
                            "fold": True,
                            "jira_status_category": "done",
                        },
                    ),
                ],
            }
        )

        # Mock response values
        cls.jira_project_id = random.randint(1000, 9999)
        sample_issues = [
            {
                "key": f"{cls.project.jira_key}-{i}",
                "id": 1000 + i,
                "fields": {
                    "summary": f"Task {i}",
                    "project": {
                        "id": cls.jira_project_id,
                        "key": cls.project.jira_key,
                    },
                    "aggregatetimeoriginalestimate": 3600 * i,
                    "duedate": "2023-01-01",
                    "issuetype": {
                        "iconUrl": f"{cls.company.atlassian_url}/sample/icon.png",
                        "hierarchyLevel": 1,
                    },
                    "status": {
                        "statusCategory": {
                            "key": "done" if i % 2 == 0 else "in_progress",
                        },
                    },
                    "assignee": {
                        "accountId": cls._jira_employee_manager.jira_account_id,
                    },
                    "updated": f"2023-01-{i:02d}T00:00:00.000+0000",
                },
            }
            for i in range(1, 6)
        ]
        cls._mock_response_values = {
            "project_validate": {
                "valid_key": cls.project.jira_key,
                "valid_name": cls.project.name,
            },
            "project": {
                "create": {"id": cls.jira_project_id, "key": cls.project.jira_key},
                "get": {
                    "id": cls.jira_project_id,
                    "key": cls.project.jira_key,
                    "name": cls.project.name + " (updated)",
                    "lead": {"accountId": cls._jira_employee_manager.jira_account_id},
                },
                "issues": {
                    "issues": sample_issues,
                },
            },
            "issue": {
                "get": sample_issues,
                "worklog": [
                    {
                        "id": si["id"],
                        "key": si["key"] + f"__WL-{idx}",
                        "author": {
                            "accountId": cls._jira_employee_manager.jira_account_id,
                            "displayName": cls._jira_employee_manager.name,
                        },
                        "comment": {
                            "content": [
                                {
                                    "type": "paragraph",
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": f"Worklog for task {si['key']}",
                                        }
                                    ],
                                }
                            ],
                        },
                        "started": f"2023-01-{idx:02d}T00:00:00.000+0000",
                        "timeSpentSeconds": 3600 * idx,
                        "updated": f"2023-01-{idx:02d}T00:00:00.000+0000",
                    }
                    for idx, si in enumerate(sample_issues, 1)
                ],
                "comments": [
                    {
                        "id": si["id"],
                        "key": si["key"] + f"__IC-{idx}",
                        "author": {
                            "accountId": cls._jira_employee_manager.jira_account_id,
                            "displayName": cls._jira_employee_manager.name,
                        },
                        "body": {
                            "content": [
                                {
                                    "type": "paragraph",
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": f"Comment for task {si['key']}",
                                        }
                                    ],
                                }
                            ],
                        },
                        "updated": f"2023-01-{idx:02d}T00:00:00.000+0000",
                    }
                    for idx, si in enumerate(sample_issues, 1)
                ],
            },
            "worklog": {
                "get_worklogs": [
                    {
                        "key": si["key"] + f"__WL-{idx}",
                        "issueId": str(si["id"]),
                        "id": str(si["id"] + idx),
                        "author": {
                            "accountId": cls._jira_employee_manager.jira_account_id,
                            "displayName": cls._jira_employee_manager.name,
                        },
                        "comment": {
                            "content": [
                                {
                                    "type": "paragraph",
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": f"Worklog for task {si['key']}",
                                        }
                                    ],
                                }
                            ],
                        },
                        "started": f"2023-01-{idx:02d}T00:00:00.000+0000",
                        "timeSpentSeconds": 3600 * idx,
                        "updated": f"2023-01-{idx:02d}T00:00:00.000+0000",
                    }
                    for idx, si in enumerate(sample_issues, 1)
                ],
                "get_updated_worklogs_ids": {
                    "values": [
                        {
                            "worklogId": si["id"] + idx,
                        }
                        for idx, si in enumerate(sample_issues, 1)
                    ]
                },
                "get_deleted_worklogs_ids": {
                    "values": [
                        {
                            "worklogId": si["id"] + idx,
                        }
                        for idx, si in enumerate(sample_issues, 1)
                    ]
                },
            },
        }
