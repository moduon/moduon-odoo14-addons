# See README.rst file on addon root folder for license details

{
    "name": "Product UX",
    "version": "16.0.1.0.2",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Sales/Sales",
    "depends": ["product"],
    "data": [
        "views/product_product_view.xml",
    ],
    "installable": False,
}
