# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo.tests import Form, common, tagged


@tagged("post_install", "-at_install")
class ResPartner(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.partner_2 = self.env["res.partner"].create(
            {
                "name": "Partner 2",
                "is_company": True,
                "child_ids": [(0, 0, {"name": "Partner 2 Contact", "type": "contact"})],
            }
        )
        self.partner_3 = self.env["res.partner"].create(
            {
                "name": "Partner 3",
                "is_company": True,
                "child_ids": [(0, 0, {"name": "Partner 3 Contact", "type": "contact"})],
            }
        )

    def test_crm_types(self):
        """Test Partner CRM Type in a middle of a Pipeline workflow"""
        cl_model = self.env["crm.lead"]
        # Without CRM Type
        self.assertEqual(self.partner_3.crm_type, False)
        self.assertEqual(self.partner_3.child_ids[0].crm_type, False)
        # Create a lead
        lead_form = Form(cl_model.with_context(default_type="lead"))
        lead_form.name = "Test Lead"
        lead_form.partner_id = self.partner_2.child_ids[0]
        lead = lead_form.save()
        self.partner_2.invalidate_recordset()
        self.assertEqual(self.partner_2.crm_type, "lead")
        # Lead is converted to opportunity
        lead.type = "opportunity"
        self.partner_2.invalidate_recordset()
        self.assertEqual(self.partner_2.crm_type, "opportunity")
        # At some point, an invoice is created and customer_rank is increased
        self.partner_2.customer_rank = 1
        self.partner_2.invalidate_recordset()
        self.assertEqual(self.partner_2.crm_type, "customer")
