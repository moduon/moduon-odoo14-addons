# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo.exceptions import UserError
from odoo.tests import Form, common, tagged


@tagged("post_install", "-at_install")
class TestCrmLead(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.partner_1 = self.env["res.partner"].create(
            {
                "name": "Partner 1",
                "is_company": True,
                "child_ids": [(0, 0, {"name": "Partner 1 Contact", "type": "contact"})],
            }
        )

    def test_commercial_partner(self):
        """Test commercial partner is set correctly"""
        lead_form = Form(self.env["crm.lead"])
        lead_form.name = "Test Lead"
        lead_form.partner_id = self.partner_1.child_ids[0]
        lead = lead_form.save()
        self.assertEqual(lead.commercial_partner_id, self.partner_1)

    def test_lost_reason(self):
        """Test lost reason cancel quotations"""
        cll_model = self.env["crm.lead.lost"]
        cl_model = self.env["crm.lead"]
        # Create Opportunity
        opportunity_form = Form(cl_model.with_context(default_type="opportunity"))
        opportunity_form.name = "Test Lead"
        opportunity_form.partner_id = self.partner_1.child_ids[0]
        opportunity = opportunity_form.save()
        # Create Quotation
        sale_form = Form(self.env["sale.order"].sudo())
        sale_form.partner_id = self.partner_1.child_ids[0]
        sale_form.opportunity_id = opportunity
        sale = sale_form.save()
        # Confirm Quotation and check that it is not possible to cancel it
        sale.action_confirm()
        lost_wiz = cll_model.create(
            {
                "lost_reason_id": self.env.ref("crm.lost_reason_1").id,
                "cancel_quotations": True,
            }
        )
        with self.assertRaises(UserError):
            lost_wiz.with_context(active_ids=opportunity.ids).action_lost_reason_apply()
        # Back Order to draft and try to cancel
        sale.with_context(disable_cancel_warning=True).action_cancel()
        sale.action_draft()
        cll_model.create(
            {
                "lost_reason_id": self.env.ref("crm.lost_reason_1").id,
                "cancel_quotations": True,
            }
        ).with_context(active_ids=opportunity.ids).action_lost_reason_apply()
        self.assertEqual(sale.state, "cancel")
