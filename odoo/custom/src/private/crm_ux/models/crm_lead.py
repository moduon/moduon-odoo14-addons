# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo import api, fields, models


class CRMLead(models.Model):
    _inherit = "crm.lead"

    partner_id = fields.Many2one(
        comodel_name="res.partner",
        domain="[('is_company', '=', False)]",
        tracking=True,
        help="This field is used to inform about the customer, either company "
        "or contact as long as it is already created in the database.\n"
        "This field searches by several fields inlcuding company, "
        "contact, email and tax identification number.\n"
        "If it does not exist, you can create it quickly but keep "
        "in mind that it will be created as a contact and you will have "
        "to associate a company to which it belongs in the creation.\n"
        "In case this field is not established and the text fields "
        "'Contact name', 'Email' and 'Company name' are filled in, "
        "when converting the initiative into an Opportunity a contact "
        "with its parent company will be automatically created taking "
        "the values of the mentioned fields.",
        string="Client contact",
    )
    commercial_partner_id = fields.Many2one(
        comodel_name="res.partner",
        string="Client",
        related="partner_id.commercial_partner_id",
        store=True,
        readonly=True,
    )
    industry_id = fields.Many2one(
        comodel_name="res.partner.industry",
        related="commercial_partner_id.industry_id",
        string="Industry",
        store=True,
        readonly=True,
    )

    @api.depends_context("uid")
    @api.depends("partner_id", "type")
    def _compute_is_partner_visible(self):
        """Partner is always visible"""
        self.is_partner_visible = True
