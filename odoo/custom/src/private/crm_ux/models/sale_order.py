from odoo import _, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def action_open_opportunity(self):
        self.ensure_one()
        return {
            "name": _("Opportunity of %s", self.name),
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "crm.lead",
            "res_id": self.opportunity_id.id,
        }
