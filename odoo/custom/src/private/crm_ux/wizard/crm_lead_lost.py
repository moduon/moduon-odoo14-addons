from odoo import _, exceptions, fields, models


class CRMLeadLost(models.TransientModel):
    _inherit = "crm.lead.lost"

    cancel_quotations = fields.Boolean(
        default=False,
        help="Try to cancel Quotations related to this opportunity.",
    )

    def action_lost_reason_apply(self):
        res = super().action_lost_reason_apply()
        # Cancel quotations
        leads = self.env["crm.lead"].browse(self.env.context.get("active_ids"))
        if self.cancel_quotations:
            if leads.order_ids.filtered(lambda o: o.state in {"sale", "done"}):
                raise exceptions.UserError(
                    _(
                        "Not all quotations can be cancelled "
                        "because some of them have been confirmed."
                    )
                )
            leads.order_ids.with_context(disable_cancel_warning=True).action_cancel()
        return res
