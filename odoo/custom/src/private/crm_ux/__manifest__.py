# See README.rst file on addon root folder for license details

{
    "name": "CRM UX",
    "version": "16.0.2.0.3",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Sales/CRM",
    "depends": [
        "sale_crm",
    ],
    "data": [
        "views/crm_lead_view.xml",
        "views/res_partner_view.xml",
        "views/sale_order_view.xml",
        "wizard/crm_lead_lost_view.xml",
    ],
    "post_init_hook": "post_init_hook",
    "installable": False,
}
