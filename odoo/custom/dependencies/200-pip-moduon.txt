# Avoid incompatibilities with upstream requirements
-c https://github.com/moduon/odoo/raw/${ODOO_VERSION}/requirements.txt
-c https://github.com/moduon/community-data-files/raw/${ODOO_VERSION}/requirements.txt
-c https://github.com/moduon/l10n-spain/raw/${ODOO_VERSION}/requirements.txt

# enterprise
asn1crypto
dbfread
ebaysdk
google_auth
lxml
python-dateutil
python-stdnum
zeep

# OCA
deepdiff
odoo-test-helper
pyjwt
requests_pkcs12
schwifty
xmlsig
xmltodict
endesive
# Openssl version not detected in current oscrypto version https://github.com/wbond/oscrypto/issues/78#issuecomment-1755515697
git+https://github.com/wbond/oscrypto.git@d5f3437 #
